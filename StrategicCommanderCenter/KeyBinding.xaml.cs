﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StrategicCommanderCenter
{
    /// <summary>
    /// Logica di interazione per KeyBinding.xaml
    /// </summary>
    public partial class KeyBinding : UserControl
    {
        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty =
          DependencyProperty.Register("Header", typeof(string), typeof(KeyBinding), 
          new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /*private static void OnHeaderChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            KeyBinding ctrl = (KeyBinding)obj;
            ctrl.Box.Header = ctrl.Header;
        }

        public static readonly DependencyProperty HeaderProperty =
          DependencyProperty.Register("HeaderProperty", typeof(string), typeof(KeyBinding),
          new FrameworkPropertyMetadata("Header", new PropertyChangedCallback(OnHeaderChanged)));*/

        public string Assignment1
        {
            get { return (string)GetValue(Assignment1Property); }
            set { SetValue(Assignment1Property, value); }
        }

        public static readonly DependencyProperty Assignment1Property =
          DependencyProperty.Register("Assignment1", typeof(string), typeof(KeyBinding),
          new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /*
        public static readonly DependencyProperty Assignment1Property =
          DependencyProperty.Register("Assignment1Property", typeof(string), typeof(KeyBinding),
          new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnAssignment1Changed)));*/

        public string Assignment2
        {
            get { return (string)GetValue(Assignment2Property); }
            set { SetValue(Assignment2Property, value); }
        }

        public static readonly DependencyProperty Assignment2Property =
          DependencyProperty.Register("Assignment2", typeof(string), typeof(KeyBinding), 
          new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        /*private static void OnAssignment2Changed(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            KeyBinding ctrl = (KeyBinding)obj;
            ctrl.AssignText2.Text = ctrl.Assignment2;
        }

        public static readonly DependencyProperty Assignment2Property =
          DependencyProperty.Register("Assignment2", typeof(string), typeof(KeyBinding),
          new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnAssignment2Changed)));*/
        
        public string Assignment3
        {
            get { return (string)GetValue(Assignment3Property); }
            set { SetValue(Assignment3Property, value); }
        }
        
        public static readonly DependencyProperty Assignment3Property =
          DependencyProperty.Register("Assignment3", typeof(string), typeof(KeyBinding), 
          new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        /*private static void OnAssignment3Changed(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            KeyBinding ctrl = (KeyBinding)obj;
            ctrl.AssignText3.Text = ctrl.Assignment3;
        }

        public static readonly DependencyProperty Assignment3Property =
          DependencyProperty.Register("Assignment3", typeof(string), typeof(KeyBinding),
          new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnAssignment3Changed)));*/

        public string Assignment4
        {
            get { return (string)GetValue(Assignment4Property); }
            set { SetValue(Assignment4Property, value); }
        }

        public static readonly DependencyProperty Assignment4Property =
          DependencyProperty.Register("Assignment4", typeof(string), typeof(KeyBinding), 
          new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /*private static void OnAssignment4Changed(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            KeyBinding ctrl = (KeyBinding)obj;
            ctrl.AssignText4.Text = ctrl.Assignment4;
        }

        public static readonly DependencyProperty Assignment4Property =
          DependencyProperty.Register("Assignment4", typeof(string), typeof(KeyBinding),
          new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnAssignment4Changed)));*/

        public bool Shift1Enabled
        {
            get { return (bool)GetValue(Shift1EnabledProperty); }
            set { SetValue(Shift1EnabledProperty, value); }
        }
        
        public static readonly DependencyProperty Shift1EnabledProperty =
          DependencyProperty.Register("Shift1Enabled", typeof(bool), typeof(KeyBinding),
          new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /*private static void OnShift1EnabledChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            KeyBinding ctrl = (KeyBinding)obj;
            ctrl.AssignText2.IsEnabled = ctrl.Shift1Enabled;
        }

        public static readonly DependencyProperty Shift1EnabledProperty =
          DependencyProperty.Register("Shift1Enabled", typeof(bool), typeof(KeyBinding),
          new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnShift1EnabledChanged)));*/

        public bool Shift2Enabled
        {
            get { return (bool)GetValue(Shift2EnabledProperty); }
            set { SetValue(Shift2EnabledProperty, value); }
        }

        public static readonly DependencyProperty Shift2EnabledProperty =
          DependencyProperty.Register("Shift2Enabled", typeof(bool), typeof(KeyBinding),
          new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /*private static void OnShift2EnabledChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            KeyBinding ctrl = (KeyBinding)obj;
            ctrl.AssignText3.IsEnabled = ctrl.Shift2Enabled;
        }

        public static readonly DependencyProperty Shift2EnabledProperty =
          DependencyProperty.Register("Shift2Enabled", typeof(bool), typeof(KeyBinding),
          new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnShift2EnabledChanged)));*/

        public bool Shift3Enabled
        {
            get { return (bool)GetValue(Shift3EnabledProperty); }
            set { SetValue(Shift3EnabledProperty, value); }
        }

        public static readonly DependencyProperty Shift3EnabledProperty =
          DependencyProperty.Register("Shift3Enabled", typeof(bool), typeof(KeyBinding), 
          new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        /*private static void OnShift3EnabledChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            KeyBinding ctrl = (KeyBinding)obj;
            ctrl.AssignText4.IsEnabled = ctrl.Shift3Enabled;
        }

        public static readonly DependencyProperty Shift3EnabledProperty =
          DependencyProperty.Register("Shift3Enabled", typeof(bool), typeof(KeyBinding),
          new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnShift3EnabledChanged)));*/

        public KeyBinding()
        {
            InitializeComponent();
        }

        public bool SetTextFocus(int index)
        {
            bool res = false;
            switch (index)
            {
                case 0:
                    res = AssignText1.Focus();
                    break;
                case 1:
                    res = AssignText2.Focus();
                    break;
                case 2:
                    res = AssignText3.Focus();
                    break;
                case 3:
                    res = AssignText4.Focus();
                    break;
            }

            return res;
        }

        private void AssignText_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            TextBox source = sender as TextBox;
            if (e.Key == Key.Back)
                source.Text = "";
            else
            {
                if (source.Text.Length == 0)
                    source.Text = AssignmentConverter.VkToString((WindowsInput.VirtualKeyCode)KeyInterop.VirtualKeyFromKey(e.Key));
                else
                    source.Text += AssignmentConverter.Separator[0] + AssignmentConverter.VkToString((WindowsInput.VirtualKeyCode)KeyInterop.VirtualKeyFromKey(e.Key));
            }
        }

    }
}
