﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace StrategicCommanderCenter
{
    /// <summary>
    /// Logica di interazione per App.xaml
    /// </summary>
    public partial class App : Application
    {
        private System.Windows.Forms.NotifyIcon tray;
        private System.Windows.Forms.ContextMenu trayMenu;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            tray = new System.Windows.Forms.NotifyIcon();
            tray.Icon = StrategicCommanderCenter.Properties.Resources.IconMain;
            tray.Text = "Strategic Commander Center";            

            trayMenu = new System.Windows.Forms.ContextMenu(new System.Windows.Forms.MenuItem[] 
            { 
                new System.Windows.Forms.MenuItem("Show Center", (s, ea) => this.MainWindow.Show()), 
                new System.Windows.Forms.MenuItem("Hide Center", (s, ea) => this.MainWindow.Hide()), 
                new System.Windows.Forms.MenuItem("-"), 
                new System.Windows.Forms.MenuItem("Close", (s, ea) => this.Shutdown()) 
            });
            tray.ContextMenu = trayMenu;
            tray.Visible = true;


            // Handle the DoubleClick event to activate the form.
            tray.DoubleClick += new System.EventHandler(this.tray_DoubleClick);

        }

        private void tray_DoubleClick(object sender, EventArgs e)
        {
            if (this.MainWindow.IsVisible)
            {
                this.MainWindow.Hide();
            }
            else
            {
                this.MainWindow.Show();
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            if (tray != null)
                tray.Dispose();
        }
    }
}
