﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategicCommanderCenter
{
    public enum StrategicShiftState : int
    {
        None = 0,
        Shift1,
        Shift2,
        Shift3
    }
}
