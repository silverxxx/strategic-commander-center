﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategicCommanderCenter
{

    public enum StrategicButtons : int
    {
        Button1 = 0,
        Button2,
        Button3,
        Button4,
        Button5,
        Button6,
        ZoomIn,
        ZoomOut,
        Shift1,
        Shift2,
        Shift3,
        Rec
    }
}
