﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsInput;

namespace StrategicCommanderCenter
{
    public class StrategicAssignment
    {
        public List<VirtualKeyCode> Codes = new System.Collections.Generic.List<VirtualKeyCode>();

        public bool IsUnassigned()
        {
            return (Codes.Count == 0);
        }
    }

    public class StrategicConfig
    {
        public StrategicProfile[] Profiles = new StrategicProfile[3];
        public int XDeadZone = 50;
        public int YDeadZone = 50;
        public int RotDeadZone = 50;

        public StrategicConfig()
        {
            Profiles[0] = new StrategicProfile() { ProfileName = "Profile 1" };
            Profiles[1] = new StrategicProfile() { ProfileName = "Profile 2" };
            Profiles[2] = new StrategicProfile() { ProfileName = "Profile 3" };
        }

        public StrategicAssignment GetAssignedLeft(StrategicProfileSelector prof)
        {
            StrategicProfile current = Profiles[(int)prof];
            return current.Left;
        }

        public StrategicAssignment GetAssignedRight(StrategicProfileSelector prof)
        {
            StrategicProfile current = Profiles[(int)prof];
            return current.Right;
        }

        public StrategicAssignment GetAssignedDown(StrategicProfileSelector prof)
        {
            StrategicProfile current = Profiles[(int)prof];
            return current.Down;
        }

        public StrategicAssignment GetAssignedUp(StrategicProfileSelector prof)
        {
            StrategicProfile current = Profiles[(int)prof];
            return current.Up;
        }

        public StrategicAssignment GetAssignedRotateLeft(StrategicProfileSelector prof)
        {
            StrategicProfile current = Profiles[(int)prof];
            return current.RotateLeft;
        }

        public StrategicAssignment GetAssignedRotateRight(StrategicProfileSelector prof)
        {
            StrategicProfile current = Profiles[(int)prof];
            return current.RotateRight;
        }

        public StrategicAssignment GetAssignedShifts(StrategicButtons shift, StrategicProfileSelector prof)
        {
            StrategicProfile current = Profiles[(int)prof];
            switch (shift)
            {
                case StrategicButtons.Shift1: return current.Shift1;
                case StrategicButtons.Shift2: return current.Shift2;
                case StrategicButtons.Shift3: return current.Shift3;
                default: throw new Exception("Wrong argument for shifts");
            }
        }

        public StrategicAssignment GetAssignedButtons(StrategicButtons button, StrategicShiftState shift, StrategicProfileSelector prof)
        {
            StrategicProfile current = Profiles[(int)prof];
            switch (button)
            {
                case StrategicButtons.Rec: return current.Rec;
                case StrategicButtons.ZoomIn: return GetZoomIn(shift, current);
                case StrategicButtons.ZoomOut: return GetZoomOut(shift, current);
                default: return GetButtons(button, shift, current);
            }
        }

        private StrategicAssignment GetButtons(StrategicButtons button, StrategicShiftState shift, StrategicProfile prof)
        {

            switch (shift)
            {
                case StrategicShiftState.None: return prof.Buttons[(int)button];
                //case StrategicShiftState.Shift1: return prof.Shift1Buttons[(int)button];
                case StrategicShiftState.Shift1: return (prof.ShiftAsModifier[0] ? prof.Shift1Buttons[(int)button] : prof.Buttons[(int)button]);
                case StrategicShiftState.Shift2: return (prof.ShiftAsModifier[1] ? prof.Shift2Buttons[(int)button] : prof.Buttons[(int)button]);
                default: //case StrategicShiftState.Shift3:
                        return (prof.ShiftAsModifier[2] ? prof.Shift3Buttons[(int)button] : prof.Buttons[(int)button]);;
            }
        }

        private StrategicAssignment GetZoomIn(StrategicShiftState shift, StrategicProfile prof)
        {
            if (!prof.ShiftZoom)
                return prof.ZoomIn;
            else
            {
                switch (shift)
                {
                    case StrategicShiftState.None: return prof.ZoomIn;
                    case StrategicShiftState.Shift1: return (prof.ShiftAsModifier[0] ?  prof.Shift1ZoomIn : prof.ZoomIn);
                    case StrategicShiftState.Shift2: return (prof.ShiftAsModifier[1] ?  prof.Shift2ZoomIn : prof.ZoomIn);
                    default: //case StrategicShiftState.Shift3:
                        return (prof.ShiftAsModifier[2] ? prof.Shift3ZoomIn : prof.ZoomIn);
                }
            }
        }

        private StrategicAssignment GetZoomOut(StrategicShiftState shift, StrategicProfile prof)
        {
            if (!prof.ShiftZoom)
                return prof.ZoomOut;
            {
                switch (shift)
                {
                    case StrategicShiftState.None: return prof.ZoomOut;
                    case StrategicShiftState.Shift1: return (prof.ShiftAsModifier[0] ?  prof.Shift1ZoomOut : prof.ZoomOut);
                    case StrategicShiftState.Shift2: return (prof.ShiftAsModifier[1] ?  prof.Shift2ZoomOut : prof.ZoomOut);
                    default: //case StrategicShiftState.Shift3:
                        return (prof.ShiftAsModifier[2] ? prof.Shift3ZoomOut : prof.ZoomOut);
                }
            }
        }
    }

    public class StrategicProfile
    {
        //private String mProfileName;
        /*private bool[] mShiftAsModifier = new bool[3] { true, true, true };

        private StrategicAssignment[] mButtons = new StrategicAssignment[6];
        private StrategicAssignment[] mShift1Buttons = new StrategicAssignment[6];
        private StrategicAssignment[] mShift2Buttons = new StrategicAssignment[6];
        private StrategicAssignment[] mShift3Buttons = new StrategicAssignment[6];

        private bool mShiftZoom = false;
        private StrategicAssignment mZoomIn;
        private StrategicAssignment mShift1ZoomIn;
        private StrategicAssignment mShift2ZoomIn;
        private StrategicAssignment mShift3ZoomIn;

        private StrategicAssignment mZoomOut;
        private StrategicAssignment mShift1ZoomOut;
        private StrategicAssignment mShift2ZoomOut;
        private StrategicAssignment mShift3ZoomOut;
        private StrategicAssignment mRec;

        private StrategicAssignment mShift1;
        private StrategicAssignment mShift2;
        private StrategicAssignment mShift3;

        private StrategicAssignment mUp;
        private StrategicAssignment mDown;
        private StrategicAssignment mLeft;
        private StrategicAssignment mRight;
        private StrategicAssignment mRotateRight;
        private StrategicAssignment mRotateLeft;*/

        public String ProfileName { get; set; }
        public bool[] ShiftAsModifier { get; set; }

        public StrategicAssignment[] Buttons { get; set; }
        public StrategicAssignment[] Shift1Buttons { get; set; }
        public StrategicAssignment[] Shift2Buttons { get; set; }
        public StrategicAssignment[] Shift3Buttons { get; set; }

        public bool ShiftZoom { get; set; }
        public StrategicAssignment ZoomIn { get; set; }
        public StrategicAssignment Shift1ZoomIn { get; set; }
        public StrategicAssignment Shift2ZoomIn { get; set; }
        public StrategicAssignment Shift3ZoomIn { get; set; }

        public StrategicAssignment ZoomOut { get; set; }
        public StrategicAssignment Shift1ZoomOut { get; set; }
        public StrategicAssignment Shift2ZoomOut { get; set; }
        public StrategicAssignment Shift3ZoomOut { get; set; }
        public StrategicAssignment Rec { get; set; }

        public StrategicAssignment Shift1 { get; set; }
        public StrategicAssignment Shift2 { get; set; }
        public StrategicAssignment Shift3 { get; set; }

        public StrategicAssignment Up { get; set; }
        public StrategicAssignment Down { get; set; }
        public StrategicAssignment Left { get; set; }
        public StrategicAssignment Right { get; set; }
        public StrategicAssignment RotateRight { get; set; }
        public StrategicAssignment RotateLeft { get; set; }

        public bool VJoy { get; set; }

        public StrategicProfile()
        {
            Buttons = new StrategicAssignment[6];
            Shift1Buttons = new StrategicAssignment[6];
            Shift2Buttons = new StrategicAssignment[6];
            Shift3Buttons = new StrategicAssignment[6];

            ShiftAsModifier = new bool[3] { true, true, true };
            for (int i = 0; i < 6; i++)
            {
                Buttons[i] = new StrategicAssignment();
                Shift1Buttons[i] = new StrategicAssignment();
                Shift2Buttons[i] = new StrategicAssignment();
                Shift3Buttons[i] = new StrategicAssignment();
            }
            ShiftZoom = false;
            Shift1ZoomIn = new StrategicAssignment();
            Shift2ZoomIn = new StrategicAssignment();
            Shift3ZoomIn = new StrategicAssignment();
            ZoomIn = new StrategicAssignment();
            Shift1ZoomOut = new StrategicAssignment();
            Shift2ZoomOut = new StrategicAssignment();
            Shift3ZoomOut = new StrategicAssignment();
            ZoomOut = new StrategicAssignment();
            Rec = new StrategicAssignment();

            Shift1 = new StrategicAssignment();
            Shift2 = new StrategicAssignment();
            Shift3 = new StrategicAssignment();

            Up = new StrategicAssignment();
            Down = new StrategicAssignment();
            Left = new StrategicAssignment();
            Right = new StrategicAssignment();
            RotateRight = new StrategicAssignment();
            RotateLeft = new StrategicAssignment();
            VJoy = false;
        }
    }
}
