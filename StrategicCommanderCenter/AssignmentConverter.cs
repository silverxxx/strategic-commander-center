﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Input;
using WindowsInput;

namespace StrategicCommanderCenter
{
    [ValueConversion(typeof(StrategicAssignment), typeof(string))]
    public sealed class AssignmentConverter : IValueConverter
    {
        private static readonly AssignmentConverter instance = new AssignmentConverter();
        public static readonly string[] Separator = new string[1] {" + "};
        private AssignmentConverter() { }
        
        public static AssignmentConverter Instance
        {
            get
            {
                return instance;
            }
        }

        public static string VkToString(VirtualKeyCode vk)
        {
            switch (vk)
            {
                case WindowsInput.VirtualKeyCode.MENU:  //Should be alt key
                    return "ALT";
                case WindowsInput.VirtualKeyCode.LMENU:  //Should be alt key
                    return "LALT";
                case WindowsInput.VirtualKeyCode.RMENU:  //Should be alt key
                    return "RALT";
                default:
                    String conv = Enum.GetName(typeof(WindowsInput.VirtualKeyCode), vk);
                    if (conv.StartsWith("VK_"))
                        conv = conv.Substring(3);
                    return conv;
            }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //From Assignment to string
            StrategicAssignment assign = value as StrategicAssignment;

            string result = "";
            foreach (WindowsInput.VirtualKeyCode vk in assign.Codes)
                result += VkToString(vk) + Separator[0];

            if (result != "")
                return result.Remove(result.Length - 3);
            else
                return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //From string to Assignment
            StrategicAssignment res = new StrategicAssignment();
            string data = value as string;
            string[] tokens = data.Split(Separator, StringSplitOptions.RemoveEmptyEntries);

            foreach (string token in tokens)
            {
                switch (token)
                {
                    case "ALT":
                            res.Codes.Add(WindowsInput.VirtualKeyCode.MENU);
                            break;
                    case "LALT":
                            res.Codes.Add(WindowsInput.VirtualKeyCode.MENU);
                            break;
                    case "RALT":
                            res.Codes.Add(WindowsInput.VirtualKeyCode.MENU);
                            break;
                    default:
                            try
                            {
                                res.Codes.Add((WindowsInput.VirtualKeyCode)Enum.Parse(typeof(WindowsInput.VirtualKeyCode), "VK_" + token.ToUpper()));
                            }
                            catch 
                            {
                                res.Codes.Add((WindowsInput.VirtualKeyCode)Enum.Parse(typeof(WindowsInput.VirtualKeyCode), token.ToUpper()));
                            }
                        break;
                }
            }

            return res;
        }
    }
}
