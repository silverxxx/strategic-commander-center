﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategicCommanderCenter
{
    public class StrategicState
    {
        public int XAxis = 0;
        public int YAxis = 0;
        public int Rotation = 0;
        public StrategicProfileSelector CurrentProfile;

        public bool[] ButtonStates = new bool[12];

        public void CopyValuesFrom(StrategicState state)
        {
            XAxis = state.XAxis;
            YAxis = state.YAxis;
            Rotation = state.Rotation;
            state.ButtonStates.CopyTo(ButtonStates, 0);
        }
    }
}
