'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 9/2/2007
' Time: 12:40 PM
'----------------------------------------------------------------------

Imports NUnit.Framework

Imports BlueNinjaSoftware.HIDLib

Namespace Devices
	
	<TestFixture> _
	Public Class DeLorme_EarthmateGPS
		
		Private GPS As HIDDevice
		
		<TestFixtureSetUp> _
		Public Sub Init
			Try
				GPS = HIDManagement.GetDevices(&h1163, &h0100, True).Item(0)
			Catch ex As Exception
				MsgBox(ex.ToString)
			End Try
		End Sub
		
		<TestFixtureTearDown> _
		Public Sub Dispose
			GPS.Dispose
		End Sub
		
		<Test> _
		Public Sub PropertiesTests
			Assert.AreEqual(&h1163, GPS.VendorID, "VendorID not correct.")
			Assert.AreEqual(&h0100, GPS.ProductID, "ProductID not correct.")
			Assert.AreEqual(&h04, GPS.VersionNumber, "VersionNumber not correct.")
			
			Assert.AreEqual("DeLorme Publishing", GPS.Manufacturer, "ManufacturerString not parsed properly.")
			Assert.AreEqual("DeLorme USB Earthmate", GPS.ProductName, "ProductString not parsed properly.")
			Assert.AreEqual(Nothing, GPS.SerialNumber, "SerialNumberString not parsed properly.")
			Assert.AreEqual("", GPS.Descriptor, "PhysicalDescriptor not parsed properly.")
			
			Assert.AreEqual("VID: 0x1163, PID: 0x0100, DeLorme Publishing DeLorme USB Earthmate rev. 0x0004, SN: ", GPS.ToString, "ToString not returned as expected.")
		End Sub
		
		<Test> _
		Public Sub ReportLengthTests
			Assert.AreEqual(33, GPS.Capabilities.InputCapabilities.ReportByteLength, "InputReportByteLength incorrect.")
			Assert.AreEqual(33, GPS.Capabilities.OutputCapabilities.ReportByteLength, "OutputReportByteLength incorrect.")
			Assert.AreEqual(6, GPS.Capabilities.FeatureCapabilities.ReportByteLength, "FeatureReportByteLength incorrect.")
		End Sub
		
		<Test> _
		Public Sub InputCapabilitiesCountTests
			Assert.AreEqual(0, GPS.Capabilities.InputCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(1, GPS.Capabilities.InputCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
			
			With GPS.Capabilities.InputCapabilities.ValueCapabilities.Item(0)
				Assert.AreEqual(&hFFA0, .UsagePage)
				
				Assert.IsTrue(.IsRange, "Usage not range as expected.")
				
				With .UsageRange
					Assert.AreEqual(0, .StringMin, "StringMin not 0.")
					Assert.AreEqual(0, .StringMax, "StringMax not 0.")
					
					Assert.AreEqual(Nothing, .GetString(0), "StringID 0 not NULL as expected.")
				End With
				
				With .SingleUsage
					Assert.AreEqual(0, .StringID, "StringID not 0.")
					
					Assert.AreEqual(Nothing, .GetString, "StringID not NULL as expected.")
				End With
			End With
		End Sub
		
		<Test> _
		Public Sub OutputCapabilitiesCountTests
			Assert.AreEqual(0, GPS.Capabilities.OutputCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(1, GPS.Capabilities.OutputCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
			
			With GPS.Capabilities.OutputCapabilities.ValueCapabilities.Item(0)
				Assert.AreEqual(&hFFA0, .UsagePage)
				
				Assert.IsTrue(.IsRange, "Usage not range as expected.")
				
				With .UsageRange
					Assert.AreEqual(0, .StringMin, "StringMin not 0.")
					Assert.AreEqual(0, .StringMax, "StringMax not 0.")
					
					Assert.AreEqual(Nothing, .GetString(0), "StringID 0 not NULL as expected.")
				End With
				
				With .SingleUsage
					Assert.AreEqual(0, .StringID, "StringID not 0.")
					
					Assert.AreEqual(Nothing, .GetString, "StringID not NULL as expected.")
				End With
			End With
		End Sub
		
		<Test> _
		Public Sub FeatureCapabilitiesTests
			Assert.AreEqual(0, GPS.Capabilities.FeatureCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(1, GPS.Capabilities.FeatureCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
			
			With GPS.Capabilities.FeatureCapabilities.ValueCapabilities.Item(0)
				Assert.AreEqual(&hFFA0, .UsagePage)
				
				Assert.IsTrue(.IsRange, "Usage not range as expected.")
				
				With .UsageRange
					Assert.AreEqual(0, .StringMin, "StringMin not 0.")
					Assert.AreEqual(0, .StringMax, "StringMax not 0.")
					
					Assert.AreEqual(Nothing, .GetString(0), "StringID 0 not NULL as expected.")
				End With
				
				With .SingleUsage
					Assert.AreEqual(0, .StringID, "StringID not 0.")
					
					Assert.AreEqual(Nothing, .GetString, "StringID not NULL as expected.")
				End With
				
			End With
		End Sub
		
		<Test> _
		Public Sub ReportTest
			'Dim O As Reports.HIDOutputReport = GPS.CreateOutputReport(0, New Byte() { &h20, &h10, &h24, &h4c, &h54, &h43, &h2c, &h31 })
			
			'If Not GPS.WriteReport(O) Then
			'	Assert.Fail("Didn't send wakeup packet: " & GPS.GetLastWin32Error.Message)
			'Else
				Dim I As Reports.HIDInputReport = GPS.ReadInputReport()
				
				If I IsNot Nothing Then
					Assert.AreEqual(0, I.ReportID)
					MsgBox(BitConverter.ToString(I.ReportData))
				Else
					Assert.Fail("Couldn't read Input report: " & GPS.GetLastWin32Error.Message)
				End If
			'End If
		End Sub
	End Class
	
End Namespace
