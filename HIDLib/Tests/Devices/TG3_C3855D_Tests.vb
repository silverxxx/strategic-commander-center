'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/28/2007
' Time: 10:09 AM
'----------------------------------------------------------------------

Imports NUnit.Framework

Imports BlueNinjaSoftware.HIDLib

Namespace Devices
	
	<TestFixture> _
	Public Class TG3_C3855D
		
		Private Kbd As HIDDevice
		
		<TestFixtureSetUp> _
		Public Sub Init
			Try
				Kbd = HIDManagement.GetDevices(&h0A34, &hB000, True).Item(0)
				'Kbd = New HIDDevice("\\?\hid#vid_0a34&pid_B000&mi_02#7&f1e20e2&0&0000#{4d1e55b2-f16f-11cf-88cb-001111000030}")
			Catch ex As Exception
				MsgBox(ex.ToString)
			End Try
		End Sub
		
		<TestFixtureTearDown> _
		Public Sub Dispose
			Kbd.Dispose
		End Sub
		
		<Test> _
		Public Sub PropertiesTests
			Assert.AreEqual(&h0A34, Kbd.VendorID, "VendorID not correct.")
			Assert.AreEqual(&hB000, Kbd.ProductID, "ProductID not correct.")
			Assert.AreEqual(&h100, Kbd.VersionNumber, "VersionNumber not correct.")
			
			Assert.AreEqual("TG3 Electronics, Inc.", Kbd.Manufacturer, "ManufacturerString not parsed properly.")
			'Assert.AreEqual("C3855 BLT", Kbd.ProductName, "ProductString not parsed properly.")
			Assert.AreEqual(Nothing, Kbd.SerialNumber, "SerialNumberString not parsed properly.")
			Assert.AreEqual("", Kbd.Descriptor, "PhysicalDescriptor not parsed properly.")
			
			Assert.AreEqual("VID: 0x0A34, PID: 0xB000, TG3 Electronics, Inc.  rev. 0x0100, SN: ", Kbd.ToString, "ToString not returned as expected.")
		End Sub
		
		<Test> _
		Public Sub ReportLengthTests
			Assert.AreEqual(0, Kbd.Capabilities.InputCapabilities.ReportByteLength, "InputReportByteLength incorrect.")
			Assert.AreEqual(0, Kbd.Capabilities.OutputCapabilities.ReportByteLength, "OutputReportByteLength incorrect.")
			Assert.AreEqual(9, Kbd.Capabilities.FeatureCapabilities.ReportByteLength, "FeatureReportByteLength incorrect.")
		End Sub
		
		<Test> _
		Public Sub InputCapabilitiesCountTests
			Assert.AreEqual(0, Kbd.Capabilities.InputCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(0, Kbd.Capabilities.InputCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
		End Sub
		
		<Test> _
		Public Sub OutputCapabilitiesCountTests
			Assert.AreEqual(0, Kbd.Capabilities.OutputCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(0, Kbd.Capabilities.OutputCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
		End Sub
		
		<Test> _
		Public Sub FeatureCapabilitiesTests
			Assert.AreEqual(1, Kbd.Capabilities.FeatureCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(0, Kbd.Capabilities.FeatureCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
			
			With Kbd.Capabilities.FeatureCapabilities.ButtonCapabilities.Item(0)
				Assert.AreEqual(&hFFA1, .UsagePage)
				
				Assert.IsTrue(.IsRange, "Usage not range as expected.")
				
				With .UsageRange
					Assert.AreEqual(0, .StringMin, "StringMin not 0.")
					Assert.AreEqual(0, .StringMax, "StringMax not 0.")
					
					Assert.AreEqual(Nothing, .GetString(0), "StringID 0 not NULL as expected.")
				End With
				
				With .SingleUsage
					Assert.AreEqual(0, .StringID, "StringID not 0.")
					
					Assert.AreEqual(Nothing, .GetString, "StringID not NULL as expected.")
				End With
				
			End With
		End Sub
		
		<Test> _
		Public Sub BacklightReportTest
			Dim W As Reports.HIDFeatureReport = Kbd.CreateFeatureReport
			
			W.ReportData(0) = 1
			W.ReportData(1) = 5
			
			Assert.IsTrue(Kbd.WriteReport(W), "Failed to write report.")
			
			W.ReportData(0) = &hFF
			W.ReportData(1) = 1
			
			Assert.IsTrue(Kbd.WriteReport(W), "Failed to write report.")
			
			Dim R As Reports.HIDFeatureReport = Kbd.ReadFeatureReport
			
			Assert.AreEqual(1, R.ReportData(0), "Backlight not off.")
			Assert.AreEqual(5, R.ReportData(1), "Backlight not off.")
		End Sub
		
		<Test> _
		Public Sub InitReportTest
			Dim F As Reports.HIDFeatureReport
			
			F = Kbd.InitializeFeatureReport(0)
			
			Assert.AreEqual("00-00-00-00-00-00-00-00-00", BitConverter.ToString(F.GetBuffer), "Feature report initialized incorrectly.")
			
			Assert.AreEqual("Report ID: 0x00, Data: 00-00-00-00-00-00-00-00", F.ToString, "Report ToString not returned properly.")
		End Sub
		
	End Class
	
End Namespace
