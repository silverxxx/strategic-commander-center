'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 9/5/2007
' Time: 3:23 PM
'----------------------------------------------------------------------

Imports NUnit.Framework

Imports BlueNinjaSoftware.HIDLib

Namespace Devices
	
	<TestFixture> _
	Public Class APC_BackUPSRS
		
		Private UPS As HIDDevice
		
		<TestFixtureSetUp> _
		Public Sub Init
			Try
				UPS = HIDManagement.GetDevices(&h051D, &h0002, True).Item(0)
			Catch ex As Exception
				MsgBox(ex.ToString)
			End Try
		End Sub
		
		<TestFixtureTearDown> _
		Public Sub Dispose
			UPS.Dispose
		End Sub
		
		<Test> _
		Public Sub PropertiesTests
			Assert.AreEqual(&h051D, UPS.VendorID, "VendorID not correct.")
			Assert.AreEqual(&h0002, UPS.ProductID, "ProductID not correct.")
			Assert.AreEqual(&h0106, UPS.VersionNumber, "VersionNumber not correct.")
			
			Assert.AreEqual("American Power Conversion", UPS.Manufacturer, "ManufacturerString not parsed properly.")
			Assert.AreEqual("Back-UPS RS 1500 FW:8.g8 .D USB FW:g8 ", UPS.ProductName, "ProductString not parsed properly.")
			Assert.AreEqual("JB0409008228  ", UPS.SerialNumber, "SerialNumberString not parsed properly.")
			Assert.AreEqual("", UPS.Descriptor, "PhysicalDescriptor not parsed properly.")
			
			Assert.AreEqual("VID: 0x051D, PID: 0x0002, American Power Conversion Back-UPS RS 1500 FW:8.g8 .D USB FW:g8  rev. 0x0106, SN: JB0409008228  ", UPS.ToString, "ToString not returned as expected.")
		End Sub
		
		<Test> _
		Public Sub ReportLengthTests
			Assert.AreEqual(5, UPS.Capabilities.InputCapabilities.ReportByteLength, "InputReportByteLength incorrect.")
			Assert.AreEqual(0, UPS.Capabilities.OutputCapabilities.ReportByteLength, "OutputReportByteLength incorrect.")
			Assert.AreEqual(5, UPS.Capabilities.FeatureCapabilities.ReportByteLength, "FeatureReportByteLength incorrect.")
		End Sub
		
		<Test> _
		Public Sub InputCapabilitiesCountTests
			Assert.AreEqual(11, UPS.Capabilities.InputCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(9, UPS.Capabilities.InputCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
			
			With UPS.Capabilities.InputCapabilities.ValueCapabilities.Item(0)
				Assert.AreEqual(133, .UsagePage)
				
				Assert.IsTrue(.IsRange, "Usage not range as expected.")
				
				With .UsageRange
					Assert.AreEqual(1, .StringMin, "StringMin not 1.")
					Assert.AreEqual(36, .StringMax, "StringMax not 36.")
					
					Assert.AreEqual(Nothing, .GetString(0), "StringID 0 not NULL as expected.")
				End With
			End With
			
		End Sub
		
		<Test> _
		Public Sub OutputCapabilitiesCountTests
			Assert.AreEqual(0, UPS.Capabilities.OutputCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(0, UPS.Capabilities.OutputCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
		End Sub
		
		<Test> _
		Public Sub FeatureCapabilitiesTests
			Assert.AreEqual(22, UPS.Capabilities.FeatureCapabilities.ButtonCapabilities.Count, "ButtonCapabilities count incorrect.")
			Assert.AreEqual(62, UPS.Capabilities.FeatureCapabilities.ValueCapabilities.Count, "ValueCapabilities count incorrect.")
			
			With UPS.Capabilities.FeatureCapabilities.ValueCapabilities.Item(0)
				Assert.AreEqual(132, .UsagePage)
				
				Assert.IsTrue(.IsRange, "Usage not range as expected.")
				
				With .UsageRange
					Assert.AreEqual(1, .StringMin, "StringMin not 1.")
					Assert.AreEqual(36, .StringMax, "StringMax not 36.")
				End With
			End With
		End Sub
		
		<Test> _
		Public Sub ReportTest
			Dim F As Reports.HIDFeatureReport = UPS.ReadFeatureReport(1)
			
			If F IsNot Nothing Then
				Assert.AreEqual(1, F.ReportID)
				MsgBox(BitConverter.ToString(F.ReportData))
			Else
				Assert.Fail("Couldn't read Feature report: " & UPS.GetLastWin32Error.Message)
			End If
		End Sub
	End Class
	
End Namespace
