'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/26/2007
' Time: 8:04 PM
'----------------------------------------------------------------------

Imports NUnit.Framework

Imports BlueNinjaSoftware.HIDLib
Imports BlueNinjaSoftware.HIDLib.Reports

<TestFixture> _
Public Class ReportTests
	
	#Region "ReportID Tests"
	
	''' <summary>
	''' Tests that the ReportID is properly initialized via the constructor, and is retrievable via the ReportID property and the buffer.
	''' </summary>
	<Test> _
	Public Sub ReportID_Constructor_Test
		Dim TestRep As New TestReport(2, 1)
		
		Assert.AreEqual(1, TestRep.ReportID, "ReportID not constructed properly.")
		Assert.AreEqual(1, TestRep.GetBuffer(0), "ReportID not constructed properly (in buffer).")
	End Sub
	
	<Test> _
	Public Sub ReportID_Assignment_Test
		Dim TestRep As New TestReport(2)
		
		TestRep.ReportID = 1
		
		Assert.AreEqual(1, TestRep.ReportID, "ReportID not assigned properly.")
		Assert.AreEqual(1, TestRep.GetBuffer(0), "ReportID not assigned properly (in buffer.")
	End Sub
	
	#End Region
	
	#Region "ReportData Tests"
	
	''' <summary>
	''' Tests that the ReportData is properly initialized via the constructor, and is retrievable via the ReportData property and the buffer.
	''' </summary>
	<Test> _
	Public Sub ReportData_Constructor_Test
		Dim TestRep As New TestReport(4, 1, New Byte() { 3, 6, 9 })
		
		'Test ReportData
		Assert.AreEqual(3, TestRep.ReportData(0), "ReportData not constructed properly.")
		Assert.AreEqual(6, TestRep.ReportData(1), "ReportData not constructed properly.")
		Assert.AreEqual(9, TestRep.ReportData(2), "ReportData not constructed properly.")
		
		'Test GetBuffer
		Assert.AreEqual(3, TestRep.GetBuffer(1), "ReportData not constructed properly (in buffer).")
		Assert.AreEqual(6, TestRep.GetBuffer(2), "ReportData not constructed properly (in buffer).")
		Assert.AreEqual(9, TestRep.GetBuffer(3), "ReportData not constructed properly (in buffer).")
	End Sub
	
	''' <summary>
	''' Tests that the ReportData property is properly set via a new byte array.
	''' </summary>
	<Test> _
	Public Sub ReportData_Assignment_Test
		Dim TestRep As New TestReport(4, 1)
		Dim NewData As Byte() = { 2, 3, 4 }
		
		TestRep.ReportData = NewData
		
		Assert.AreEqual(2, TestRep.ReportData(0), "ReportData not assigned properly.")
		Assert.AreEqual(3, TestRep.ReportData(1), "ReportData not assigned properly.")
		Assert.AreEqual(4, TestRep.ReportData(2), "ReportData not assigned properly.")
	End Sub
	
	''' <summary>
	''' Tests that the ReportData property is properly set via the indexer.
	''' </summary>
	<Test> _
	Public Sub ReportData_Indexer_Test
		Dim TestRep As New TestReport(4, 1)
		
		TestRep.ReportData(0) = 2
		TestRep.ReportData(1) = 3
		TestRep.ReportData(2) = 4
		
		Assert.AreEqual(2, TestRep.ReportData(0), "ReportData not assigned properly.")
		Assert.AreEqual(3, TestRep.ReportData(1), "ReportData not assigned properly.")
		Assert.AreEqual(4, TestRep.ReportData(2), "ReportData not assigned properly.")
	End Sub
	
	#End Region
	
	#Region "Misc. Buffer Tests"
	
	''' <summary>
	''' Tests that writing a new, shorter ReportData array will clear out the remaining buffer.
	''' </summary>
	<Test> _
	Public Sub ReportData_ShorterDataClearsOriginalBuffer_Test
		Dim TestRep As New TestReport(4, 1, New Byte() { 3, 6, 9 })
		Dim NewData As Byte() = { 7, 8 }
		
		TestRep.ReportData = NewData
		
		Assert.AreEqual(0, TestRep.GetBuffer(3), "Old buffer not cleared when writing shorter data.")
	End Sub
	
	<Test> _
	Public Sub Buffer_WriteViaGetBuffer_Test
		Dim TestRep As New TestReport(4, 1)
		
		TestRep.GetBuffer(1) = 3
		
		Assert.AreEqual(3, TestRep.GetBuffer(1), "Buffer not written properly via GetBuffer.")
	End Sub
	
	<Test> _
	Public Sub Buffer_ViaConstructor_Test
		Dim RepBuff As Byte() = { 1, 2, 3, 4 }
		Dim TestRep As New TestReport(RepBuff)
		
		Assert.AreEqual(1, TestRep.ReportID, "ReportID not constructed from buffer.")
		Assert.AreEqual(2, TestRep.ReportData(0), "ReportData(0) not constructed from buffer.")
		Assert.AreEqual(3, TestRep.ReportData(1), "ReportData(1) not constructed from buffer.")
		Assert.AreEqual(4, TestRep.ReportData(2), "ReportData(2) not constructed from buffer.")
		
		Array.Reverse(RepBuff)
		
		Assert.AreEqual(1, TestRep.ReportID, "Buffer constructed with reference, not copy.")
		Assert.AreEqual(2, TestRep.ReportData(0), "Buffer constructed with reference, not copy.")
		Assert.AreEqual(3, TestRep.ReportData(1), "Buffer constructed with reference, not copy.")
		Assert.AreEqual(4, TestRep.ReportData(2), "Buffer constructed with reference, not copy.")
	End Sub
	
	<Test> _
	Public Sub Report_Equality_Test
		Dim Rep1 As New TestReport(4, 1, New Byte() { 3, 6, 9 })
		Dim Rep2 As New TestReport(4, 1, New Byte() { 3, 6, 9 })
		Dim Rep3 As New TestReport(4, 2, New Byte() { 4, 6, 8 })
		
		Assert.IsTrue(Rep1.Equals(Rep2), "Equal reports failed equality test.")
		Assert.IsFalse(Rep1.Equals(Rep3), "Unequal reports passed equality test.")
	End Sub
	
	#End Region
	
	#Region "Invalid Data Tests"
	
	''' <summary>
	''' Tests that specifying too large a ReportData byte array in the constructor will trigger an InvalidOperation exception.
	''' </summary>
	<Test> _
	<ExpectedException(GetType(InvalidOperationException))> _
	Public Sub InvalidReportData_TooLarge_Constructor_Test
		Dim TestRep As New TestReport(4, 1, New Byte() { 2, 4, 6, 8 })
	End Sub
	
	''' <summary>
	''' Tests that writing too large a ReportData byte array will trigger an InvalidOperation exception.
	''' </summary>
	<Test> _
	<ExpectedException(GetType(InvalidOperationException))> _
	Public Sub InvalidReportData_TooLarge_Property_Test
		Dim TestRep As New TestReport(4)
		Dim TestData(4) As Byte
		
		TestRep.ReportData = TestData
	End Sub
	
	''' <summary>
	''' Tests that specifying an invalid indexer for the ReportData byte array will trigger an InvalidOperation exception.
	''' </summary>
	<Test> _
	<ExpectedException(GetType(InvalidOperationException))> _
	Public Sub InvalidReportData_Index_Test
		Dim TestRep As New TestReport(4)
		
		TestRep.ReportData(4) = 1
	End Sub
	
	#End Region
	
End Class

''' <summary>
''' Inherited to give us a device-independant constructor.
''' </summary>
Friend Class TestReport
	Inherits AHIDReport
	
	Public Sub New(ByVal ReportLength As UShort)
		MyBase.New(ReportLength)
	End Sub
	
	Public Sub New(ByVal ReportLength As UShort, ByVal ReportID As Byte)
		MyBase.New(ReportLength, ReportID)
	End Sub
	
	Public Sub New(ByVal ReportLength As UShort, ByVal ReportID As Byte, ByVal ReportData As Byte())
		MyBase.New(ReportLength, ReportID, ReportData)
	End Sub
	
	Public Sub New(ByVal ReportBuffer As Byte())
		MyBase.New(ReportBuffer)
	End Sub
	
End Class