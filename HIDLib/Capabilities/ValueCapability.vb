'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/28/2007
' Time: 9:27 AM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	Public Class ValueCapability
		Inherits ACapability
		
		Private _ValueCaps As HIDP_VALUE_CAPS
		
		Friend Sub New(ByRef Parent As ValueCapabilities, ByVal ValueCaps As HIDP_VALUE_CAPS)
			MyBase.New(Parent, ValueCaps.Range, ValueCaps.NotRange)
			
			_ValueCaps = ValueCaps
		End Sub
		
	    ''' <summary>Specifies the usage page of the usage or usage range.</summary>
	    Public ReadOnly Property UsagePage As UShort
	    	Get
	    		Return _ValueCaps.UsagePage
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the report ID of the HID report that contains the usage or usage range.</summary>
	    Public ReadOnly Property ReportID As SByte
	    	Get
	    		Return _ValueCaps.ReportID
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates, if TRUE, that the usage is member of a set of aliased usages. Otherwise, if IsAlias is FALSE, the value has only one usage.</summary>
	    Public ReadOnly Property IsAlias As Boolean
	    	Get
	    		Return _ValueCaps.IsAlias
	    	End Get
	    End Property
	    
	    ''' <summary>Contains the data fields (one or two bytes) associated with an input, output, or feature main item.</summary>
	    Public ReadOnly Property BitField As UShort
	    	Get
	    		Return _ValueCaps.BitField
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the index of the link collection in a top-level collection's link collection array that contains the usage or usage range. If LinkCollection is zero, the usage or usage range is contained in the top-level collection.</summary>
	    Public ReadOnly Property LinkCollection As UShort
	    	Get
	    		Return _ValueCaps.LinkCollection
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the usage of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsage specifies the usage of the top-level collection.</summary>
	    Public ReadOnly Property LinkUsage As Short
	    	Get
	    		Return _ValueCaps.LinkUsage
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the usage page of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsagePage specifies the usage page of the top-level collection.</summary>
	    Public ReadOnly Property LinkUsagePage As Short
	    	Get
	    		Return _ValueCaps.LinkUsagePage
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the structure describes a usage range. Otherwise, if IsRange is FALSE, the structure describes a single usage.</summary>
	    Public ReadOnly Property IsRange As Boolean
	    	Get
	    		Return _ValueCaps.IsRange
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of string descriptors. Otherwise, if IsStringRange is FALSE, the usage or usage range has zero or one string descriptor.</summary>
	    Public ReadOnly Property IsStringRange As Boolean
	    	Get
	    		Return _ValueCaps.IsStringRange
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of designators. Otherwise, if IsDesignatorRange is FALSE, the usage or usage range has zero or one designator.</summary>
	    Public ReadOnly Property IsDesignatorRange As Boolean
	    	Get
	    		Return _ValueCaps.IsDesignatorRange
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range provides absolute data. Otherwise, if IsAbsolute is FALSE, the value is the change in state from the previous value.</summary>
	    Public ReadOnly Property IsAbsolute As Boolean
	    	Get
	    		Return _ValueCaps.IsAbsolute
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the usage supports a NULL value, which indicates that the data is not valid and should be ignored. Otherwise, if HasNull is FALSE, the usage does not have a NULL value.</summary>
	    Public HasNull As Boolean
	    
	    ''' <summary>Specifies the size, in bits, of a usage's data field in a report. If ReportCount is greater than one, each usage has a separate data field of this size.</summary>
	    Public BitSize As UShort
	    
	    ''' <summary>Specifies the number of usages that this structure describes.</summary>
	    Public ReportCount As UShort
	    
	    ''' <summary>Specifies the usage's exponent, as described by the USB HID standard.</summary>
	    Public UnitsExp As Integer
	    
	    ''' <summary>Specifies the usage's units, as described by the USB HID Standard.</summary>
	    Public Units As Integer
	    
	    ''' <summary>Specifies a usage's signed lower bound.</summary>
	    Public LogicalMin As Integer
	    
	    ''' <summary>Specifies a usage's signed upper bound.</summary>
	    Public LogicalMax As Integer
	    
	    ''' <summary>Specifies a usage's signed lower bound after scaling is applied to the logical minimum value.</summary>
	    Public PhysicalMin As Integer
	    
	    ''' <summary>Specifies a usage's signed upper bound after scaling is applied to the logical maximum value.</summary>
	    Public PhysicalMax As Integer
	    
	End Class
	
End Namespace
