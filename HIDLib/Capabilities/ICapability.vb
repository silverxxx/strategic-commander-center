'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 9/8/2007
' Time: 2:55 PM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	Public Interface ICapability
	
	    ''' <summary>Specifies the usage page of the usage or usage range.</summary>
	    ReadOnly Property UsagePage As UShort
	    
	    ''' <summary>Specifies the report ID of the HID report that contains the usage or usage range.</summary>
	    ReadOnly Property ReportID As SByte
	    
	    ''' <summary>Indicates, if TRUE, that the usage is member of a set of aliased usages. Otherwise, if IsAlias is FALSE, the value has only one usage.</summary>
	    ReadOnly Property IsAlias As Boolean
	    
	    ''' <summary>Contains the data fields (one or two bytes) associated with an input, output, or feature main item.</summary>
	    ReadOnly Property BitField As UShort
	    
	    ''' <summary>Specifies the index of the link collection in a top-level collection's link collection array that contains the usage or usage range. If LinkCollection is zero, the usage or usage range is contained in the top-level collection.</summary>
	    ReadOnly Property LinkCollection As UShort
	    
	    ''' <summary>Specifies the usage of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsage specifies the usage of the top-level collection.</summary>
	    ReadOnly Property LinkUsage As Short
	    
	    ''' <summary>Specifies the usage page of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsagePage specifies the usage page of the top-level collection.</summary>
	    ReadOnly Property LinkUsagePage As Short
	    
	    ''' <summary>Specifies, if TRUE, that the structure describes a usage range. Otherwise, if IsRange is FALSE, the structure describes a single usage.</summary>
	    ReadOnly Property IsRange As Boolean
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of string descriptors. Otherwise, if IsStringRange is FALSE, the usage or usage range has zero or one string descriptor.</summary>
	    ReadOnly Property IsStringRange As Boolean
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of designators. Otherwise, if IsDesignatorRange is FALSE, the usage or usage range has zero or one designator.</summary>
	    ReadOnly Property IsDesignatorRange As Boolean
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range provides absolute data. Otherwise, if IsAbsolute is FALSE, the value is the change in state from the previous value.</summary>
	    ReadOnly Property IsAbsolute As Boolean
		
		''' <summary>Specifies, if IsRange is TRUE, information about a usage range. Otherwise, if IsRange is FALSE, NotRange contains information about a single usage.</summary>
		ReadOnly Property UsageRange As UsageRange
		
		''' <summary>Specifies, if IsRange is FALSE, information about a single usage. Otherwise, if IsRange is TRUE, Range contains information about a usage range.</summary>
		ReadOnly Property SingleUsage As SingleUsage
		
	End Interface
	
End Namespace
