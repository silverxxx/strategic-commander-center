'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/28/2007
' Time: 7:49 AM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	''' <summary>
	''' Specifies, if IsRange is TRUE, information about a usage range. Otherwise, if IsRange is FALSE, NotRange contains information about a single usage.
	''' </summary>
	Public Class UsageRange
	    Private _Parent As ACapability
	    
	    Private _Range As RangeStruct
	    
	    Friend Sub New(ByRef Parent As ACapability, ByVal Range As RangeStruct)
	    	_Parent = Parent
	    	_Range = Range
	    End Sub
	    
	    ''' <summary>Indicates the inclusive lower bound of usage range whose inclusive upper bound is specified by Range.UsageMax.</summary>
	    Public ReadOnly Property UsageMin As UShort
	    	Get
	    		Return _Range.UsageMin
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates the inclusive upper bound of a usage range whose inclusive lower bound is indicated by Range.UsageMin.</summary>
	    Public ReadOnly Property UsageMax As UShort
	    	Get
	    		Return _Range.UsageMax
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates the inclusive lower bound of a range of string descriptors (specified by string minimum and string maximum items) whose inclusive upper bound is indicated by Range.StringMax.</summary>
	    Public ReadOnly Property StringMin As UShort
	    	Get
	    		Return _Range.StringMin
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates the inclusive upper bound of a range of string descriptors (specified by string minimum and string maximum items) whose inclusive lower bound is indicated by Range.StringMax.</summary>
	    Public ReadOnly Property StringMax As UShort
	    	Get
	    		Return _Range.StringMax
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates the inclusive lower bound of a range of designators (specified by designator minimum and designator maximum items) whose inclusive upper bound is indicated by Range.DesignatorMax.</summary>
	    Public ReadOnly Property DesignatorMin As UShort
	    	Get
	    		Return _Range.DesignatorMin
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates the inclusive upper bound of a range of designators (specified by designator minimum and designator maximum items) whose inclusive lower bound is indicated by Range.DesignatorMax.</summary>
	    Public ReadOnly Property DesignatorMax As UShort
	    	Get
	    		Return _Range.DesignatorMax
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates the inclusive lower bound of a sequential range of data indices that correspond, one-to-one and in the same order, to the usages specified by the usage range Range.UsageMin to Range.UsageMax.</summary>
	    Public ReadOnly Property DataIndexMin As UShort
	    	Get
	    		Return _Range.DataIndexMin
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates the inclusive upper bound of a sequential range of data indices that correspond, one-to-one and in the same order, to the usages specified by the usage range Range.UsageMin to Range.UsageMax.</summary>
	    Public ReadOnly Property DataIndexMax As UShort
	    	Get
	    		Return _Range.DataIndexMax
	    	End Get
	    End Property
	    
		Public Function GetString(ByVal StringID As UShort) As String
			'If StringID >= _Range.StringMin AndAlso StringID <= _Range.StringMax Then
				Dim Buff As String = StrDup(127, Chr(0))
				
				If HidD_GetIndexedString(_Parent._Parent._Parent._Parent._Parent.SafeHandle.DangerousGetHandle, StringID, Buff, Buff.Length * 2) Then
					Return ParseString(Buff)
				End If
			'Else
			'	Throw New ArgumentException("StringID is out of range.")
			'End If
		End Function
	    
	End Class
	
	''' <summary>
	''' Specifies, if IsRange is FALSE, information about a single usage. Otherwise, if IsRange is TRUE, Range contains information about a usage range.
	''' </summary>
	Public Class SingleUsage
		
		Private _Parent As ACapability
		
		Private _NotRange As NotRangeStruct
		
		Friend Sub New(ByRef Parent As ACapability, ByVal NotRange As NotRangeStruct)
			_Parent = Parent
			_NotRange = NotRange
		End Sub
		
		''' <summary>Indicates a usage ID.</summary>
		Public ReadOnly Property Usage As UShort
			Get
				Return _NotRange.Usage
			End Get
		End Property
		
		''' <summary>Indicates a string descriptor ID for the usage specified by NotRange.Usage.</summary>
		Public ReadOnly Property StringID As UShort
			Get
				Return _NotRange.StringID
			End Get
		End Property
		
		''' <summary>Indicates a designator ID for the usage specified by NotRange.Usage.</summary>
		Public ReadOnly Property DesignatorID As UShort
			Get
				Return _NotRange.DesignatorID
			End Get
		End Property
		
		''' <summary>Indicates the data index of the usage specified by NotRange.Usage.</summary>
		Public ReadOnly Property DataIndex As UShort
			Get
				Return _NotRange.DataIndex
			End Get
		End Property
		
		Public Function GetString() As String
			Dim Buff As String = StrDup(127, Chr(0))
			
			If HidD_GetIndexedString(_Parent._Parent._Parent._Parent._Parent.SafeHandle.DangerousGetHandle, _NotRange.StringID, Buff, Buff.Length * 2) Then
				Return ParseString(Buff)
			End If
		End Function
		
	End Class
	
End Namespace
