'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 9/5/2007
' Time: 12:20 PM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	''' <summary>
	''' Abstract base class for a Cabability - ButtonCapability or ValueCapability.
	''' </summary>
	Public MustInherit Class ACapability
		
		'Implements ICapability
		
		Friend _Parent As ACapabilities
		
		'Private _Capability As ICapability
		
		Private _UsageRange As UsageRange
		Private _SingleUsage As SingleUsage
		
		Friend Sub New(ByRef Parent As ACapabilities, ByVal CapsRange As RangeStruct, ByVal CapsNotRange As NotRangeStruct)
			_Parent = Parent
			
			_UsageRange = New UsageRange(Me, CapsRange)
			_SingleUsage = New SingleUsage(Me, CapsNotRange)
			
		End Sub
		
		
		
		''' <summary>Specifies, if IsRange is TRUE, information about a usage range. Otherwise, if IsRange is FALSE, NotRange contains information about a single usage.</summary>
	    Public ReadOnly Property UsageRange As UsageRange
	    	Get
	    		Return _UsageRange
	    	End Get
	    End Property
	    
		''' <summary>Specifies, if IsRange is FALSE, information about a single usage. Otherwise, if IsRange is TRUE, Range contains information about a usage range.</summary>
	    Public ReadOnly Property SingleUsage As SingleUsage
	    	Get
	    		Return _SingleUsage
	    	End Get
	    End Property
		
	End Class
	
End Namespace
