'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/28/2007
' Time: 6:56 AM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	''' <summary>
	''' The HIDP_CAPS structure contains information about a top-level collection's capability.
	''' </summary>
	Public Class DeviceCapabilities
		
		Friend _Parent As HIDDevice
		
		Private _Caps As HIDP_CAPS
		
		Private _InputCaps As DeviceSubCapabilities
		Private _OutputCaps As DeviceSubCapabilities
		Private _FeatureCaps As DeviceSubCapabilities
		
		Friend Sub New(ByRef Parent As HIDDevice, ByVal PHIDP_PREPARSED_DATA As IntPtr)
			Dim rc As HID.NTSTATUS = HidP_GetCaps(PHIDP_PREPARSED_DATA, _Caps)
			
			If rc < HID.NTSTATUS.NT_WARNING Then
				Dim InValueCaps As HIDP_VALUE_CAPS()
				Dim InButtonCaps As HIDP_BUTTON_CAPS()
				
				Dim OutValueCaps As HIDP_VALUE_CAPS()
				Dim OutButtonCaps As HIDP_BUTTON_CAPS()
				
				Dim FeatValueCaps As HIDP_VALUE_CAPS()
				Dim FeatButtonCaps As HIDP_BUTTON_CAPS()
				
				_Parent = Parent
				
				If _Caps.NumberInputValueCaps > 0 Then
					ReDim InValueCaps(_Caps.NumberInputValueCaps - 1)
					
					rc = HidP_GetValueCaps(HID.HIDP_REPORT_TYPE.Input, InValueCaps, _Caps.NumberInputValueCaps, PHIDP_PREPARSED_DATA)
					If rc >= HID.NTSTATUS.NT_WARNING Then
						MsgBox("Can't get input value caps: " & rc.ToString)
					End If
				End If
				
				If _Caps.NumberInputButtonCaps > 0 Then
					ReDim InButtonCaps(_Caps.NumberInputButtonCaps - 1)
					
					rc =HidP_GetButtonCaps(HID.HIDP_REPORT_TYPE.Input, InButtonCaps, _Caps.NumberInputButtonCaps, PHIDP_PREPARSED_DATA)
					If rc >= HID.NTSTATUS.NT_WARNING Then
						MsgBox("Can't get input button caps: " & rc.ToString)
					End If
				End If
				
				_InputCaps = New DeviceSubCapabilities(Me, _Caps.InputReportByteLength, _Caps.NumberInputDataIndices, InButtonCaps, InValueCaps)
				
				If _Caps.NumberOutputValueCaps > 0 Then
					ReDim OutValueCaps(_Caps.NumberOutputValueCaps - 1)
					
					rc = HidP_GetValueCaps(HID.HIDP_REPORT_TYPE.Output, OutValueCaps, _Caps.NumberOutputValueCaps, PHIDP_PREPARSED_DATA)
					If rc >= HID.NTSTATUS.NT_WARNING Then
						MsgBox("Can't get output value caps: " & rc.ToString)
					End If
				End If
				
				If _Caps.NumberOutputButtonCaps > 0 Then
					ReDim OutButtonCaps(_Caps.NumberOutputButtonCaps - 1)
					
					rc = HidP_GetButtonCaps(HID.HIDP_REPORT_TYPE.Output, OutButtonCaps, _Caps.NumberOutputButtonCaps, PHIDP_PREPARSED_DATA)
					If rc >= HID.NTSTATUS.NT_WARNING Then
						MsgBox("Can't get output button caps: " & rc.ToString)
					End If
				End If
				
				_OutputCaps = New DeviceSubCapabilities(Me, _Caps.OutputReportByteLength, _Caps.NumberOutputDataIndices, OutButtonCaps, OutValueCaps)
				
				If _Caps.NumberFeatureValueCaps > 0 Then
					ReDim FeatValueCaps(_Caps.NumberFeatureValueCaps - 1)
					
					rc = HidP_GetValueCaps(HID.HIDP_REPORT_TYPE.Feature, FeatValueCaps, _Caps.NumberFeatureValueCaps, PHIDP_PREPARSED_DATA)
					If rc >= HID.NTSTATUS.NT_WARNING Then
						MsgBox("Can't get feature value caps: " & rc.ToString)
					End If
				End If
				
				If _Caps.NumberFeatureButtonCaps > 0 Then
					ReDim FeatButtonCaps(_Caps.NumberFeatureButtonCaps - 1)
					
					rc = HidP_GetButtonCaps(HID.HIDP_REPORT_TYPE.Feature, FeatButtonCaps, _Caps.NumberFeatureButtonCaps, PHIDP_PREPARSED_DATA)
					If rc >= HID.NTSTATUS.NT_WARNING Then
						MsgBox("Can't get feature button caps: " & rc.ToString)
					End If
				End If
				
				_FeatureCaps = New DeviceSubCapabilities(Me, _Caps.FeatureReportByteLength, _Caps.NumberFeatureDataIndices, FeatButtonCaps, FeatValueCaps)
			Else
				Throw New ApplicationException("Could not get capabilities: " & rc.ToString)
			End If
		End Sub
		
	    ''' <summary>Specifies a top-level collection's usage ID.</summary>
	    Public ReadOnly Property Usage As UShort
	    	Get
	    		Return _Caps.Usage
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the top-level collection's usage page.</summary>
	    Public ReadOnly Property UsagePage As UShort
	    	Get
	    		Return _Caps.UsagePage
	    	End Get
	    End Property
	    
	    Public ReadOnly Property InputCapabilities As DeviceSubCapabilities
	    	Get
	    		Return _InputCaps
	    	End Get
	    End Property
	    
	    Public ReadOnly Property OutputCapabilities As DeviceSubCapabilities
	    	Get
	    		Return _OutputCaps
	    	End Get
	    End Property
	    
	    Public ReadOnly Property FeatureCapabilities As DeviceSubCapabilities
	    	Get
	    		Return _FeatureCaps
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the number of HIDP_LINK_COLLECTION_NODE structures that are returned for this top-level collection by HidP_GetLinkCollectionNodes.</summary>
	    Public ReadOnly Property NumberLinkCollectionNodes As UShort
	    	Get
	    		Return _Caps.NumberLinkCollectionNodes
	    	End Get
	    End Property
		
	End Class
	
End Namespace
