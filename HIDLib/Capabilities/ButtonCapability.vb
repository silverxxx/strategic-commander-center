'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/28/2007
' Time: 9:27 AM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	Public Class ButtonCapability
		Inherits ACapability
		
		Private _ButtonCaps As HIDP_BUTTON_CAPS
		
		Friend Sub New(ByRef Parent As ButtonCapabilities, ByVal ButtonCaps As HIDP_BUTTON_CAPS)
			MyBase.New(Parent, ButtonCaps.Range, ButtonCaps.NotRange)
			
			_ButtonCaps = ButtonCaps
		End Sub
		
	    ''' <summary>Specifies the usage page of the usage or usage range.</summary>
	    Public ReadOnly Property UsagePage As UShort
	    	Get
	    		Return _ButtonCaps.UsagePage
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the report ID of the HID report that contains the usage or usage range.</summary>
	    Public ReadOnly Property ReportID As SByte
	    	Get
	    		Return _ButtonCaps.ReportID
	    	End Get
	    End Property
	    
	    ''' <summary>Indicates, if TRUE, that a button has a set of aliased usages. Otherwise, if IsAlias is FALSE, the button has only one usage.</summary>
	    Public ReadOnly Property IsAlias As Boolean
	    	Get
	    		Return _ButtonCaps.IsAlias
	    	End Get
	    End Property
	    
	    ''' <summary>Contains the data fields (one or two bytes) associated with an input, output, or feature main item.</summary>
	    Public ReadOnly Property BitField As UShort
	    	Get
	    		Return _ButtonCaps.BitField
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the index of the link collection in a top-level collection's link collection array that contains the usage or usage range. If LinkCollection is zero, the usage or usage range is contained in the top-level collection.</summary>
	    Public ReadOnly Property LinkCollection As UShort
	    	Get
	    		Return _ButtonCaps.LinkCollection
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the usage of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsage specifies the usage of the top-level collection.</summary>
	    Public ReadOnly Property LinkUsage As Short
	    	Get
	    		Return _ButtonCaps.LinkUsage
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies the usage page of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsagePage specifies the usage page of the top-level collection.</summary>
	    Public ReadOnly Property LinkUsagePage As Short
	    	Get
	    		Return _ButtonCaps.LinkUsagePage
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the structure describes a usage range. Otherwise, if IsRange is FALSE, the structure describes a single usage.</summary>
	    Public ReadOnly Property IsRange As Boolean
	    	Get
	    		Return _ButtonCaps.IsRange
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of string descriptors. Otherwise, if IsStringRange is FALSE, the usage or usage range has zero or one string descriptor.</summary>
	    Public ReadOnly Property IsStringRange As Boolean
	    	Get
	    		Return _ButtonCaps.IsStringRange
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of designators. Otherwise, if IsDesignatorRange is FALSE, the usage or usage range has zero or one designator.</summary>
	    Public ReadOnly Property IsDesignatorRange As Boolean
	    	Get
	    		Return _ButtonCaps.IsDesignatorRange
	    	End Get
	    End Property
	    
	    ''' <summary>Specifies, if TRUE, that the usage or usage range provides absolute data. Otherwise, if IsAbsolute is FALSE, the value is the change in state from the previous value.</summary>
	    Public ReadOnly Property IsAbsolute As Boolean
	    	Get
	    		Return _ButtonCaps.IsAbsolute
	    	End Get
	    End Property
		
	End Class
	
End Namespace
