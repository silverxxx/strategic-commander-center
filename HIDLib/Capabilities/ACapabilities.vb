'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 9/5/2007
' Time: 1:18 PM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	''' <summary>
	''' Abstract base class for a Capabilities set - ButtonCapabilities or ValueCapabilities.
	''' </summary>
	Public MustInherit Class ACapabilities
		
		Implements IEnumerable
		
		Friend _Parent As DeviceSubCapabilities
		
		Friend _CapsList As New List(Of ACapability)
		
		Protected Sub New(ByRef Parent As DeviceSubCapabilities)
			_Parent = Parent
		End Sub
		
	    ''' <summary>Specifies the total number of HIDP_BUTTONS_CAPS structures of this type that HidP_GetButtonCaps returns.</summary>
		Public ReadOnly Property Count As UShort
			Get
				Return _CapsList.Count
			End Get
		End Property
		
		Protected Overridable ReadOnly Default Property Item(ByVal Index As UShort) As ACapability
			Get
				Return _CapsList.Item(Index)
			End Get
		End Property
		
		Public Function GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
			Return _CapsList.GetEnumerator
		End Function
		
	End Class
	
End Namespace
