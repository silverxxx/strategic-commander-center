'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/28/2007
' Time: 7:39 AM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	''' <summary>
	''' The HIDP_BUTTON_CAPS structure contains information about the capability of a HID control button usage (or a set of buttons associated with a usage range).
	''' </summary>
	Public Class ButtonCapabilities
		Inherits ACapabilities
		
		Friend Sub New(ByRef Parent As DeviceSubCapabilities, ByVal ButtonCaps As HIDP_BUTTON_CAPS())
			MyBase.New(Parent)
			
			If ButtonCaps IsNot Nothing Then
				For i As Integer = 0 To ButtonCaps.Length - 1
					MyBase._CapsList.Add(New ButtonCapability(Me, ButtonCaps(i)))
				Next
			End If
		End Sub
		
		Public Shadows ReadOnly Default Property Item(ByVal Index As UShort) As ButtonCapability
			Get
				Return MyBase.Item(Index)
			End Get
		End Property
		
	End Class
	
End Namespace
