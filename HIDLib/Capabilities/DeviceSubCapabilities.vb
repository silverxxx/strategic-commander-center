'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/28/2007
' Time: 7:23 AM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	''' <summary>
	''' Contains the capabilities specific to a subset - Input, Output, or Feature.
	''' </summary>
	Public Class DeviceSubCapabilities
	    
	    Friend _Parent As DeviceCapabilities
	    
	    Private _RepLength As UShort
	    Private _NumDataIndices As UShort
	    
		Private _ButtonCaps As ButtonCapabilities
		Private _ValueCaps As ValueCapabilities
	    
		Friend Sub New(ByRef Parent As DeviceCapabilities, ByVal RepLength As UShort, ByVal NumDataIndices As UShort, ByVal ButtonCaps As HIDP_BUTTON_CAPS(), ByVal ValueCaps As HIDP_VALUE_CAPS())
			_Parent = Parent
			
			_RepLength = RepLength
			_NumDataIndices = NumDataIndices
			
			_ButtonCaps = New ButtonCapabilities(Me, ButtonCaps)
			_ValueCaps = New ValueCapabilities(Me, ValueCaps)
		End Sub
	    
	    ''' <summary>Specifies the maximum size, in bytes, of all the reports of this type (including the report ID, if report IDs are used, which is prepended to the report data).</summary>
	    Public ReadOnly Property ReportByteLength As UShort
	    	Get
	    		Return _RepLength
	    	End Get
	    End Property
		
	    ''' <summary>Specifies the number of data indices assigned to buttons and values in all reports of this type.</summary>
	    Public ReadOnly Property NumberDataIndices As UShort
	    	Get
	    		Return _NumDataIndices
	    	End Get
	    End Property
		
		Public ReadOnly Property ValueCapabilities As ValueCapabilities
			Get
				Return _ValueCaps
			End Get
		End Property
		
		Public ReadOnly Property ButtonCapabilities As ButtonCapabilities
			Get
				Return _ButtonCaps
			End Get
		End Property
		
	End Class
	
End Namespace
