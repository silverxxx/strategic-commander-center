'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/28/2007
' Time: 6:59 AM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Interop

Namespace Capabilities
	
	''' <summary>
	''' The HIDP_VALUE_CAPS structure contains information that describes the capability of a set of HID control values (either a single usage or a usage range).
	''' </summary>
	Public Class ValueCapabilities
		Inherits ACapabilities '(Of ValueCapability)
		
		Friend Sub New(ByRef Parent As DeviceSubCapabilities, ByVal ValueCaps As HIDP_VALUE_CAPS())
			MyBase.New(Parent)
			
			If ValueCaps IsNot Nothing Then
				For i As Integer = 0 To ValueCaps.Length - 1
					MyBase._CapsList.Add(New ValueCapability(Me, ValueCaps(i)))
				Next
			End If
		End Sub
		
		Public Shadows ReadOnly Default Property Item(ByVal Index As UShort) As ValueCapability
			Get
				Return MyBase.Item(Index)
			End Get
		End Property
		
	End Class
	
End Namespace
