﻿'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/22/2007
' Time: 12:38 PM
'----------------------------------------------------------------------

Imports System.Runtime.InteropServices

Namespace Interop
	
	''' <summary>
	''' Interop declarations from hid.dll.
	''' </summary>
	Friend Module HID
		
		#Region "Enums"
		
		''' <summary>
		''' The HIDP_REPORT_TYPE enumeration type is used to specify an HID report type.
		''' </summary>
		Public Enum HIDP_REPORT_TYPE As Short
		    
		    ''' <summary>Indicates an input report.</summary>
		    Input = 0
		    
		    ''' <summary>Indicates an output report.</summary>
		    Output = 1
		    
		    ''' <summary>Indicates a feature report.</summary>
		    Feature = 2
		End Enum
		
		''' <summary>
		''' 
		''' </summary>
		Public Enum NTSTATUS As UInteger
			'TODO: Get values!
			
			''' <summary>The STATUS_BUFFER_TOO_SMALL NTSTATUS error value indicates that the buffer is too small to contain the entry. No information is written to the buffer.</summary>
			BUFFER_TOO_SMALL
			
			
			'HID status codes
			
			INVALID_PREPARSED_DATA
			INVALID_REPORT_LENGTH
			INVALID_REPORT_TYPE
			DATA_INDEX_NOT_FOUND
			REPORT_DOES_NOT_EXIST
			
			'Flags
			
			NT_INFORMATION	= &h40000000
			NT_WARNING		= &h8000000 * 2
			NT_ERROR		= &hC000000 * 2
			
		End Enum
		
		#End Region
	    
		#Region "Structures"
		
		''' <summary>
		''' The HIDD_ATTRIBUTES structure contains vendor information about a HIDClass device.
		''' </summary>
		''' <remarks>A caller of HidD_GetAttributes, uses this structure to obtain a device's vendor information.
		''' 	<para>Before using a HIDD_ATTRIBUTES structure with HIDClass support routines, the caller must set the Size member.</para>
		''' </remarks>
		<StructLayout(LayoutKind.Sequential)> _
		Friend Structure HIDD_ATTRIBUTES
		    
		    ''' <summary>Specifies the size, in bytes, of a HIDD_ATTRIBUTES structure.</summary>
		    Public Size As UInteger
		    
		    ''' <summary>Specifies a HID device's vendor ID.</summary>
		    Public VendorID As UShort
		    
		    ''' <summary>Specifies a HID device's product ID.</summary>
		    Public ProductID As UShort
		    
		    ''' <summary>Specifies the manufacturer's revision number for a HIDClass device.</summary>
		    Public VersionNumber As UShort
		    
		End Structure
		
		''' <summary>
		''' The HIDP_CAPS structure contains information about a top-level collection's capability.
		''' </summary>
		''' <remarks>Callers of the HIDClass support routines use the information provided in this structure when a called routine requires, as input, the size of a report type, the number of link collection nodes, the number of control capabilities, or the number of data indices.</remarks>
		<StructLayout(LayoutKind.Sequential)> _
		Friend Structure HIDP_CAPS
		    
		    ''' <summary>Specifies a top-level collection's usage ID.</summary>
		    Public Usage As UShort
		    
		    ''' <summary>Specifies the top-level collection's usage page.</summary>
		    Public UsagePage As UShort
		    
		    ''' <summary>Specifies the maximum size, in bytes, of all the input reports (including the report ID, if report IDs are used, which is prepended to the report data).</summary>
		    Public InputReportByteLength As UShort
		    
		    ''' <summary>Specifies the maximum size, in bytes, of all the output reports (including the report ID, if report IDs are used, which is prepended to the report data).</summary>
		    Public OutputReportByteLength As UShort
		    
		    ''' <summary>Specifies the maximum length, in bytes, of all the feature reports (including the report ID, if report IDs are used, which is prepended to the report data).</summary>
		    Public FeatureReportByteLength As UShort
		    
		    ''' <summary>Reserved for internal system use.</summary>
		    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=17)> _
		    Public Reserved As UShort()
		    
		    ''' <summary>Specifies the number of HIDP_LINK_COLLECTION_NODE structures that are returned for this top-level collection by HidP_GetLinkCollectionNodes.</summary>
		    Public NumberLinkCollectionNodes As UShort
		    
		    ''' <summary>Specifies the number of input HIDP_BUTTON_CAPS structures that HidP_GetButtonCaps returns.</summary>
		    Public NumberInputButtonCaps As UShort
		    
		    ''' <summary>Specifies the number of input HIDP_VALUE_CAPS structures that HidP_GetValueCaps returns.</summary>
		    Public NumberInputValueCaps As UShort
		    
		    ''' <summary>Specifies the number of data indices assigned to buttons and values in all input reports.</summary>
		    Public NumberInputDataIndices As UShort
		    
		    ''' <summary>Specifies the number of output HIDP_BUTTON_CAPS structures that HidP_GetButtonCaps returns.</summary>
		    Public NumberOutputButtonCaps As UShort
		    
		    ''' <summary>Specifies the number of output HIDP_VALUE_CAPS structures that HidP_GetValueCaps returns.</summary>
		    Public NumberOutputValueCaps As UShort
		    
		    ''' <summary>Specifies the number of data indices assigned to buttons and values in all output reports.</summary>
		    Public NumberOutputDataIndices As UShort
		    
		    ''' <summary>Specifies the total number of feature HIDP_BUTTONS_CAPS structures that HidP_GetButtonCaps returns.</summary>
		    Public NumberFeatureButtonCaps As UShort
		    
		    ''' <summary>Specifies the total number of feature HIDP_VALUE_CAPS structures that HidP_GetValueCaps returns.</summary>
		    Public NumberFeatureValueCaps As UShort
		    
		    ''' <summary>Specifies the number of data indices assigned to buttons and values in all feature reports.</summary>
		    Public NumberFeatureDataIndices As UShort
		
		End Structure
		
		''' <summary>
		''' The HIDP_VALUE_CAPS structure contains information that describes the capability of a set of HID control values (either a single usage or a usage range).
		''' </summary>
		''' <remarks>Clients obtain a value capability array by calling HidP_GetValueCaps or HidP_GetSpecificValueCaps. These routines return an array of HIDP_VALUE_CAPS structures in a caller-allocated buffer. The required buffer length is specified in the HIDP_CAPS structure returned by HidP_GetCaps.
		''' 	<para>For information about the capabilities of HID control values, see Collection Capability and Value Capability Arrays.</para>
		''' </remarks>
		<StructLayout(LayoutKind.Explicit)> _
		Friend Structure HIDP_VALUE_CAPS
		    
		    'NOTE: StructLayout.Explicit and FieldOffset used to allow for union at end.
		    
		    ''' <summary>Specifies the usage page of the usage or usage range.</summary>
		    <FieldOffset(0)>	Public UsagePage As UShort
		    
		    ''' <summary>Specifies the report ID of the HID report that contains the usage or usage range.</summary>
		    <FieldOffset(2)>	Public ReportID As SByte
		    
		    ''' <summary>Indicates, if TRUE, that the usage is member of a set of aliased usages. Otherwise, if IsAlias is FALSE, the value has only one usage.</summary>
		    <FieldOffset(3)>	Public IsAlias As Boolean
		    
		    ''' <summary>Contains the data fields (one or two bytes) associated with an input, output, or feature main item.</summary>
		    <FieldOffset(7)>	Public BitField As UShort
		    
		    ''' <summary>Specifies the index of the link collection in a top-level collection's link collection array that contains the usage or usage range. If LinkCollection is zero, the usage or usage range is contained in the top-level collection.</summary>
		    <FieldOffset(9)>	Public LinkCollection As UShort
		    
		    ''' <summary>Specifies the usage of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsage specifies the usage of the top-level collection.</summary>
		    <FieldOffset(11)>	Public LinkUsage As Short
		    
		    ''' <summary>Specifies the usage page of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsagePage specifies the usage page of the top-level collection.</summary>
		    <FieldOffset(13)>	Public LinkUsagePage As Short
		    
		    ''' <summary>Specifies, if TRUE, that the structure describes a usage range. Otherwise, if IsRange is FALSE, the structure describes a single usage.</summary>
		    <FieldOffset(15)>	Public IsRange As Boolean
		    
		    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of string descriptors. Otherwise, if IsStringRange is FALSE, the usage or usage range has zero or one string descriptor.</summary>
		    <FieldOffset(19)>	Public IsStringRange As Boolean
		    
		    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of designators. Otherwise, if IsDesignatorRange is FALSE, the usage or usage range has zero or one designator.</summary>
		    <FieldOffset(23)>	Public IsDesignatorRange As Boolean
		    
		    ''' <summary>Specifies, if TRUE, that the usage or usage range provides absolute data. Otherwise, if IsAbsolute is FALSE, the value is the change in state from the previous value.</summary>
		    <FieldOffset(27)>	Public IsAbsolute As Boolean
		    
		    ''' <summary>Specifies, if TRUE, that the usage supports a NULL value, which indicates that the data is not valid and should be ignored. Otherwise, if HasNull is FALSE, the usage does not have a NULL value.</summary>
		    <FieldOffset(31)>	Public HasNull As Boolean
		    
		    ''' <summary>Reserved for internal system use.</summary>
		    <FieldOffset(35)>	Public Reserved As Byte
		    
		    ''' <summary>Specifies the size, in bits, of a usage's data field in a report. If ReportCount is greater than one, each usage has a separate data field of this size.</summary>
		    <FieldOffset(36)>	Public BitSize As UShort
		    
		    ''' <summary>Specifies the number of usages that this structure describes.</summary>
		    <FieldOffset(38)>	Public ReportCount As UShort
		    
		    ''' <summary>Reserved for internal system use.</summary>
		    <MarshalAs(UnmanagedType.ByValArray, SizeConst := 5)> _
		    <FieldOffset(40)>	Public Reserved2 As UShort()
		    
		    ''' <summary>Specifies the usage's exponent, as described by the USB HID standard.</summary>
		    <FieldOffset(50)>	Public UnitsExp As Integer
		    
		    ''' <summary>Specifies the usage's units, as described by the USB HID Standard.</summary>
		    <FieldOffset(54)>	Public Units As Integer
		    
		    ''' <summary>Specifies a usage's signed lower bound.</summary>
		    <FieldOffset(58)>	Public LogicalMin As Integer
		    
		    ''' <summary>Specifies a usage's signed upper bound.</summary>
		    <FieldOffset(62)>	Public LogicalMax As Integer
		    
		    ''' <summary>Specifies a usage's signed lower bound after scaling is applied to the logical minimum value.</summary>
		    <FieldOffset(66)>	Public PhysicalMin As Integer
		    
		    ''' <summary>Specifies a usage's signed upper bound after scaling is applied to the logical maximum value.</summary>
		    <FieldOffset(70)>	Public PhysicalMax As Integer
		    
			''' <summary>Specifies, if IsRange is TRUE, information about a usage range. Otherwise, if IsRange is FALSE, NotRange contains information about a single usage.</summary>
		    <FieldOffset(74)>	Public Range As RangeStruct
		    
			''' <summary>Specifies, if IsRange is FALSE, information about a single usage. Otherwise, if IsRange is TRUE, Range contains information about a usage range.</summary>
		    <FieldOffset(74)>	Public NotRange As NotRangeStruct
		    
		End Structure
		
		''' <summary>
		''' The HIDP_BUTTON_CAPS structure contains information about the capability of a HID control button usage (or a set of buttons associated with a usage range).
		''' </summary>
		''' <remarks>Clients obtain a button capability array by calling HidP_GetButtonCaps or HidP_GetSpecificButtonCaps. These routines return an array of HIDP_BUTTON_CAPS structures in a caller-allocated buffer. The required buffer length is specified in the HIDP_CAPS structure returned by HidP_GetCaps.
		''' 	<para>For information about the capabilities of HID control values, see Collection Capability and Value Capability Arrays.</para>
		''' </remarks>
		<StructLayout(LayoutKind.Explicit)> _
		Friend Structure HIDP_BUTTON_CAPS
			
		    'NOTE: StructLayout.Explicit and FieldOffset used to allow for union at end.
		    
		    ''' <summary>Specifies the usage page of the usage or usage range.</summary>
		    <FieldOffset(0)>	Public UsagePage As UShort
		    
		    ''' <summary>Specifies the report ID of the HID report that contains the usage or usage range.</summary>
		    <FieldOffset(2)>	Public ReportID As SByte
		    
		    ''' <summary>Indicates, if TRUE, that the usage is member of a set of aliased usages. Otherwise, if IsAlias is FALSE, the value has only one usage.</summary>
		    <FieldOffset(3)>	Public IsAlias As Boolean
		    
		    ''' <summary>Contains the data fields (one or two bytes) associated with an input, output, or feature main item.</summary>
		    <FieldOffset(7)>	Public BitField As UShort
		    
		    ''' <summary>Specifies the index of the link collection in a top-level collection's link collection array that contains the usage or usage range. If LinkCollection is zero, the usage or usage range is contained in the top-level collection.</summary>
		    <FieldOffset(9)>	Public LinkCollection As UShort
		    
		    ''' <summary>Specifies the usage of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsage specifies the usage of the top-level collection.</summary>
		    <FieldOffset(11)>	Public LinkUsage As Short
		    
		    ''' <summary>Specifies the usage page of the link collection that contains the usage or usage range. If LinkCollection is zero, LinkUsagePage specifies the usage page of the top-level collection.</summary>
		    <FieldOffset(13)>	Public LinkUsagePage As Short
		    
		    ''' <summary>Specifies, if TRUE, that the structure describes a usage range. Otherwise, if IsRange is FALSE, the structure describes a single usage.</summary>
		    <FieldOffset(15)>	Public IsRange As Boolean
		    
		    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of string descriptors. Otherwise, if IsStringRange is FALSE, the usage or usage range has zero or one string descriptor.</summary>
		    <FieldOffset(19)>	Public IsStringRange As Boolean
		    
		    ''' <summary>Specifies, if TRUE, that the usage or usage range has a set of designators. Otherwise, if IsDesignatorRange is FALSE, the usage or usage range has zero or one designator.</summary>
		    <FieldOffset(23)>	Public IsDesignatorRange As Boolean
		    
		    ''' <summary>Specifies, if TRUE, that the usage or usage range provides absolute data. Otherwise, if IsAbsolute is FALSE, the value is the change in state from the previous value.</summary>
		    <FieldOffset(27)>	Public IsAbsolute As Boolean
			
			'NOTE: The field offset leaves a 1-byte "hole" here because pointers must be evenly aligned to a 4-byte boundary, apparently.
			
			''' <summary>Reserved for internal system use.</summary>
			<MarshalAs(UnmanagedType.ByValArray, SizeConst := 10)> _
			<FieldOffset(32)>	Public Reserved As UInteger()
			
			''' <summary>Specifies, if IsRange is TRUE, information about a usage range. Otherwise, if IsRange is FALSE, NotRange contains information about a single usage.</summary>
		    <FieldOffset(72)>	Public Range As RangeStruct
		    
			''' <summary>Specifies, if IsRange is FALSE, information about a single usage. Otherwise, if IsRange is TRUE, Range contains information about a usage range.</summary>
		    <FieldOffset(72)>	Public NotRange As NotRangeStruct
			
		End Structure
		
		#End Region
		
	    #Region "Union Member Structures"
	    
	    ''' <summary>
	    ''' Specifies, if IsRange is TRUE, information about a usage range. Otherwise, if IsRange is FALSE, NotRange contains information about a single usage.
	    ''' </summary>
	    <StructLayout(LayoutKind.Sequential)> _
	    Friend Structure RangeStruct
	        
	        ''' <summary>Indicates the inclusive lower bound of usage range whose inclusive upper bound is specified by Range.UsageMax.</summary>
	        Public UsageMin As UShort
	        
	        ''' <summary>Indicates the inclusive upper bound of a usage range whose inclusive lower bound is indicated by Range.UsageMin.</summary>
	        Public UsageMax As UShort
	        
	        ''' <summary>Indicates the inclusive lower bound of a range of string descriptors (specified by string minimum and string maximum items) whose inclusive upper bound is indicated by Range.StringMax.</summary>
	        Public StringMin As UShort
	        
	        ''' <summary>Indicates the inclusive upper bound of a range of string descriptors (specified by string minimum and string maximum items) whose inclusive lower bound is indicated by Range.StringMax.</summary>
	        Public StringMax As UShort
	        
	        ''' <summary>Indicates the inclusive lower bound of a range of designators (specified by designator minimum and designator maximum items) whose inclusive upper bound is indicated by Range.DesignatorMax.</summary>
	        Public DesignatorMin As UShort
	        
	        ''' <summary>Indicates the inclusive upper bound of a range of designators (specified by designator minimum and designator maximum items) whose inclusive lower bound is indicated by Range.DesignatorMax.</summary>
	        Public DesignatorMax As UShort
	        
	        ''' <summary>Indicates the inclusive lower bound of a sequential range of data indices that correspond, one-to-one and in the same order, to the usages specified by the usage range Range.UsageMin to Range.UsageMax.</summary>
	        Public DataIndexMin As UShort
	        
	        ''' <summary>Indicates the inclusive upper bound of a sequential range of data indices that correspond, one-to-one and in the same order, to the usages specified by the usage range Range.UsageMin to Range.UsageMax.</summary>
	        Public DataIndexMax As UShort
	        
	    End Structure
	    
	    ''' <summary>
	    ''' Specifies, if IsRange is FALSE, information about a single usage. Otherwise, if IsRange is TRUE, Range contains information about a usage range.
	    ''' </summary>
	    <StructLayout(LayoutKind.Sequential)> _
	    Friend Structure NotRangeStruct
	    	
	    	''' <summary>Indicates a usage ID.</summary>
	    	Public Usage As UShort
	    	
	    	''' <summary>Reserved for internal system use.</summary>
	    	Public Reserved1 As UShort
	    	
	    	''' <summary>Indicates a string descriptor ID for the usage specified by NotRange.Usage.</summary>
	    	Public StringID As UShort
	    	
	    	''' <summary>Reserved for internal system use.</summary>
	    	Public Reserved2 As UShort
	    	
	    	''' <summary>Indicates a designator ID for the usage specified by NotRange.Usage.</summary>
	    	Public DesignatorID As UShort
	    	
	    	''' <summary>Reserved for internal system use.</summary>
	    	Public Reserved3 As UShort
	    	
	    	''' <summary>Indicates the data index of the usage specified by NotRange.Usage.</summary>
	    	Public DataIndex As UShort
	    	
	    	''' <summary>Reserved for internal system use.</summary>
	    	Public Reserved4 As UShort
	    	
	    End Structure
	    
	    #End Region
	    
	    #Region "Imports (General)"
	    
	    ''' <summary>
	    ''' The HidD_GetHidGuid routine returns the device interface GUID for HIDClass devices.
	    ''' </summary>
	    ''' <param name="HidGuid">Pointer to a caller-allocated GUID buffer that the routine uses to return the device interface GUID for HIDClass devices.</param>
	    ''' <remarks>Only user-mode applications can call HidD_GetHidGuid.</remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Sub HidD_GetHidGuid(<Out()> ByRef HidGuid As System.Guid)
		End Sub
		
		''' <summary>
		''' The HidD_GetNumInputBuffers routine returns the current size, in number of reports, of the ring buffer that the HID class driver uses to queue input reports from a specified top-level collection.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="NumberBuffers">Pointer to a caller-allocated variable that the routine uses to return the maximum number of input reports the ring buffer can hold.</param>
		''' <returns>HidD_GetNumInputBuffers returns TRUE if it succeeds; otherwise, it returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_GetNumInputBuffers. Kernel-mode drivers can use the IOCTL_GET_NUM_DEVICE_INPUT_BUFFERS request.</remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_GetNumInputBuffers(ByVal HidDeviceObject As Integer, ByRef NumberBuffers As UInteger) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_SetNumInputBuffers routine sets the maximum number of input reports that the HID class driver ring buffer can hold for a specified top-level collection.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="NumberBuffers">Specifies the maximum number of buffers that the HID class driver should maintain for the input reports generated by the HidDeviceObject collection.</param>
		''' <returns>HidD_SetNumInputBuffers returns TRUE if it succeeds; otherwise, it returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_SetNumInputBuffers. Kernel-mode drivers must use an IOCTL_SET_NUM_DEVICE_INPUT_BUFFERS request.
		''' 	<para>If HidD_SetNumInputBuffers returns FALSE, and the Microsoft Win32 GetLastError function indicates that an invalid parameter was supplied, the value of NumberBuffers is invalid. The HID class driver requires a minimum of two input buffers. On Windows 2000, the maximum number of input buffers that the HID class driver supports is 200, and on Windows XP and later, the maximum number of input buffers that the HID class driver supports is 512. The default number of input buffers is 32.</para>
		''' </remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_SetNumInputBuffers(ByVal HidDeviceObject As Integer, 	ByVal NumberBuffers As UInteger)	As Boolean
		End Function
		
		''' <summary>
		''' The HidD_FlushQueue routine deletes all pending input reports in a top-level collection's input queue.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to the top-level collection whose input queue is flushed.</param>
		''' <returns>HidD_FlushQueue returns TRUE if it successfully flushes the queue. Otherwise, it returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_FlushQueue. Kernel-mode drivers can use an IOCTL_HID_FLUSH_QUEUE request.</remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_FlushQueue(ByVal HidDeviceObject As Integer) As Boolean
		End Function
	    
	    #End Region
	    
		#Region "Imports (Preparsed Data - Attributes & Capabilities)"
		
		''' <summary>
		''' The HidD_GetPreparsedData routine returns a top-level collection's preparsed data.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="PreparsedData">Pointer to the address of a routine-allocated buffer that contains a collection's preparsed data in a _HIDP_PREPARSED_DATA structure.</param>
		''' <returns>HidD_GetPreparsedData returns TRUE if it succeeds; otherwise, it returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_GetPreparsedData. Kernel-mode drivers can use an IOCTL_HID_GET_COLLECTION_DESCRIPTOR request.
		''' 	<para>When an application no longer requires the preparsed data, it should call HidD_FreePreparsedData to free the preparsed data buffer.</para>
		''' </remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_GetPreparsedData(ByVal HidDeviceObject As Integer, ByRef PreparsedData As IntPtr) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_FreePreparsedData routine releases the resources that the HID class driver allocated to hold a top-level collection's preparsed data.
		''' </summary>
		''' <param name="PreparsedData">Pointer to the buffer, returned by HidD_GetPreparsedData, that is freed.</param>
		''' <returns>HidD_FreePreparsedData returns TRUE if it succeeds. Otherwise, it returns FALSE if the buffer was not a preparsed data buffer.</returns>
		''' <remarks>Only user-mode applications can call HidD_FreePreparsedData.</remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_FreePreparsedData(ByRef PreparsedData As IntPtr) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_GetAttributes routine returns the attributes of a specified top-level collection.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="Attributes">Pointer to a caller-allocated HIDD_ATTRIBUTES structure that returns the attributes of the collection specified by HidDeviceObject.</param>
		''' <returns>HidD_GetAttributes returns TRUE if succeeds; otherwise, it returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_GetAttributes. Kernel-mode drivers can use IOCTL_HID_GET_COLLECTION_INFORMATION.</remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_GetAttributes(ByVal HidDeviceObject As Integer, ByRef Attributes As HIDD_ATTRIBUTES) As Boolean
		End Function
		
		''' <summary>
		''' The HidP_GetCaps routine returns a top-level collection's HIDP_CAPS structure.
		''' </summary>
		''' <param name="PreparsedData">Pointer to a top-level collection's preparsed data.</param>
		''' <param name="Capabilities">Pointer to a caller-allocated buffer that the routine uses to return a collection's HIDP_CAPS structure.</param>
		''' <returns>HidP_GetCaps returns one of the following status values:
		''' 	<list type="table">
		''' 		<item>
		''' 			<term>HIDP_STATUS_SUCCESS</term>
		''' 			<description>The routine successfully returned the collection capability information.</description>
		''' 		</item>
		''' 		<item>
		''' 			<term>HIDP_STATUS_INVALID_PREPARSED_DATA</term>
		''' 			<description>The specified preparsed data is invalid.</description>
		''' 		</item>
		''' 	</list>
		''' </returns>
		''' <remarks>HidD_GetPreparsedData runs at IRQL = PASSIVE_LEVEL.</remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidP_GetCaps(ByVal PreparsedData As IntPtr, ByRef Capabilities As HIDP_CAPS) As NTSTATUS
		End Function
		
		''' <summary>
		''' The HidP_GetValueCaps routine returns a value capability array that describes all the HID control values in a top-level collection for a specified type of HID report.
		''' </summary>
		''' <param name="ReportType">Specifies a HIDP_REPORT_TYPE enumerator value that identifies the report type.</param>
		''' <param name="ValueCaps">Pointer to a caller-allocated buffer in which the routine returns a value capability array for the specified report type.</param>
		''' <param name="ValueCapsLength">Specifies the length, on input, in array elements, of the ValueCaps buffer. On output, the routine sets ValueCapsLength to the number of elements that the it actually returns.</param>
		''' <param name="PreparsedData">Pointer to a top-level collection's preparsed data.</param>
		''' <returns>HidP_GetValueCaps returns one of the following status values:
		''' 	<list type="table">
		''' 		<item>
		''' 			<term>HIDP_STATUS_SUCCESS</term>
		''' 			<description>The routine successfully returned the capability data.</description>
		''' 		</item>
		''' 		<item>
		''' 			<term>HIDP_STATUS_INVALID_PREPARSED_DATA</term>
		''' 			<description>The preparsed data is not valid.</description>
		''' 		</item>
		''' 	</list>
		''' </returns>
		''' <remarks>The correct length for ValueCapsLength is specified by the NumberXxxValueCaps members of a top-level collection's HIDP_CAPS structure.
		''' 	<para>HidP_GetValueCaps runs at IRQL &lt;= DISPATCH_LEVEL.</para>
		''' </remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidP_GetValueCaps(ByVal ReportType As HIDP_REPORT_TYPE, <MarshalAs(UnmanagedType.LPArray)> <Out> ByVal ValueCaps As HIDP_VALUE_CAPS(), ByRef ValueCapsLength As UInteger, ByVal PreparsedData As IntPtr) As NTSTATUS
		End Function
		
		''' <summary>
		''' The HidP_GetButtonCaps routine returns a button capability array that describes all the HID control buttons in a top-level collection for a specified type of HID report.
		''' </summary>
		''' <param name="ReportType">Specifies a HIDP_REPORT_TYPE enumerator value that identifies the report type.</param>
		''' <param name="ButtonCaps">Pointer to a caller-allocated buffer that the routine uses to return a button capability array for the specified report type.</param>
		''' <param name="ButtonCapsLength">Specifies the length on input, in array elements, of the buffer provided at ButtonCaps. On output, this parameter is set to the actual number of elements that the routine returns.</param>
		''' <param name="PreparsedData">Pointer to a top-level collection's preparsed data.</param>
		''' <returns>HidP_GetButtonCaps returns one of the following status values:
		''' 	<list type="table">
		''' 		<item>
		''' 			<term>HIDP_STATUS_SUCCESS</term>
		''' 			<description>The routine successfully returned the capability data.</description>
		''' 		</item>
		''' 		<item>
		''' 			<term>HIDP_STATUS_INVALID_PREPARSED_DATA</term>
		''' 			<description>The preparsed data is not valid.</description>
		''' 		</item>
		''' 	</list>
		''' </returns>
		''' <remarks>HidP_GetButtonCaps returns the capability of all buttons in a top level collection for a specified report type.
		''' 	<para>ButtonCapsLength should be set to the value of the NumberXxxButtonCaps member of the HIDP_CAPS structure returned by HidP_GetCaps, where Xxx specifies the report type.</para>
		''' 	<para>To obtain a subset of button capabilities, selected by usage, usage page, or link collection, use HidP_GetSpecificButtonCaps.</para>
		''' 	<para>HidP_GetButtonCaps runs at IRQL = PASSIVE_LEVEL.</para>
		''' </remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidP_GetButtonCaps(ByVal ReportType As HIDP_REPORT_TYPE, <MarshalAs(UnmanagedType.LPArray)> <Out> ByVal ButtonCaps As HIDP_BUTTON_CAPS(), ByRef ButtonCapsLength As UInteger, ByVal PreparsedData As IntPtr) As NTSTATUS
		End Function
		
		#End Region
		
		#Region "Imports (Input/Output Reports)"
		
		''' <summary>
		''' The HidD_GetInputReport routine returns an input report from a top-level collection.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="lpReportBuffer">Pointer to a caller-allocated input report buffer that the caller uses to specify a HID report ID and HidD_GetInputReport uses to return the specified input report.
		''' 	<para>If the top-level collection includes report IDs, the caller must set the first byte of the buffer to a nonzero report ID; otherwise the caller must set the first byte to zero.</para>
		''' </param>
		''' <param name="ReportBufferLength">Specifies the size, in bytes, of the report buffer. The report buffer must be large enough to hold the input report — excluding its report ID, if report IDs are used — plus one additional byte that specifies a nonzero report ID or zero.</param>
		''' <returns>HidD_GetInputReport returns TRUE if it succeeds; otherwise, it returns FALSE.</returns>
		''' <remarks>The input report — excluding its report ID, if report IDs are used — is returned at ((PUCHAR)ReportBuffer + 1).
		''' 	<para>Only user-mode applications can call HidD_GetInputReport. Kernel-mode drivers can use an IOCTL_HID_GET_INPUT_REPORT request.</para>
		''' </remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_GetInputReport(ByVal HidDeviceObject As Integer, <[In], Out> ByVal lpReportBuffer As Byte(), ByVal ReportBufferLength As UInteger) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_SetOutputReport routine sends an output report to a top-level collection.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="lpReportBuffer">Pointer to a caller-allocated output report buffer that the caller uses to specify a report ID.
		''' 	<para>If the top-level collection includes report IDs, the caller must set the first byte of the buffer to a nonzero report ID; otherwise, the caller must set the first byte to zero.</para>
		''' 	<para>The output report — excluding its report ID, if report IDs are used — is located at ((PUCHAR)ReportBuffer + 1).</para>
		''' </param>
		''' <param name="ReportBufferLength">Specifies the size, in bytes, of the report buffer. The report buffer must be large enough to hold the output report — excluding its report ID, if report IDs are used — plus one additional byte that specifies a nonzero report ID or zero.</param>
		''' <returns>If HidD_SetOutputReport succeeds, it returns TRUE; otherwise, it returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_SetOutputReport. Kernel-mode drivers can use an IOCTL_HID_SET_OUTPUT_REPORT request.</remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_SetOutputReport(ByVal HidDeviceObject As Integer, <[In]> ByVal lpReportBuffer As Byte(), ByVal ReportBufferLength As UInteger) As Boolean
		End Function
		
		#End Region
		
		#Region "Imports (Feature Reports)"
		
		''' <summary>
		''' The HidD_GetFeature routine returns a feature report from a specified top-level collection.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="lpReportBuffer">Pointer to a caller-allocated HID report buffer that the caller uses to specify a report ID. HidD_GetFeature uses ReportBuffer to return the specified feature report.
		''' 	<para>If the top-level collection includes report IDs, the caller must set the first byte of the buffer to a nonzero report ID; otherwise the caller must set the first byte to zero.</para>
		''' </param>
		''' <param name="ReportBufferLength">Specifies the size, in bytes, of the report buffer. The report buffer must be large enough to hold the feature report — excluding its report ID, if report IDs are used — plus one additional byte that specifies a nonzero report ID or zero.</param>
		''' <returns>If HidD_GetFeature succeeds, it returns TRUE; otherwise, it returns FALSE.</returns>
		''' <remarks>The feature report — excluding its report ID, if report IDs are used — is returned at ((PUCHAR)ReportBuffer + 1).
		''' 	<para>Only user-mode applications can call HidD_GetFeature. Kernel-mode drivers can use an IOCTL_HID_GET_FEATURE request.</para>
		''' </remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_GetFeature(ByVal HidDeviceObject As Integer, <[In], Out> ByVal lpReportBuffer As Byte(), ByVal ReportBufferLength As UInteger) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_SetFeature routine sends a feature report to a top-level collection.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="lpReportBuffer">Pointer to a caller-allocated feature report buffer that the caller uses to specify a HID report ID.
		''' 	<para>If the top-level collection includes report IDs, the caller must set the first byte of the buffer to a nonzero report ID; otherwise, the caller must set the first byte to zero.</para>
		''' 	<para>The feature report — excluding its report ID, if report IDs are used — is located at ((PUCHAR)ReportBuffer + 1).</para>
		''' </param>
		''' <param name="ReportBufferLength">Specifies the size, in bytes, of the report buffer. The report buffer must be large enough to hold the feature report — excluding its report ID, if report IDs are used — plus one additional byte that specifies a nonzero report ID or zero.</param>
		''' <returns>If HidD_SetFeature succeeds, it returns TRUE; otherwise, it returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_SetFeature. Kernel-mode drivers can use the IOCTL_HID_SET_FEATURE request.</remarks>
		<DllImport("hid.dll", SetLastError := True)> _
		Function HidD_SetFeature(ByVal HidDeviceObject As Integer, <[In]> ByVal lpReportBuffer As Byte(), ByVal ReportBufferLength As UInteger) As Boolean
		End Function
		
		#End Region
	    
	    #Region "Imports (Report Initialization)"
	    
	    ''' <summary>
	    ''' The HidP_InitializeReportForID routine initializes a HID report.
	    ''' </summary>
	    ''' <param name="ReportType">Specifies a HIDP_REPORT_TYPE enumerator that indicates the type of HID report located at Report.</param>
	    ''' <param name="ReportID">Specifies a report ID.</param>
	    ''' <param name="PreparsedData">Pointer to the preparsed data of the top-level collection associated with the HID report located at Report.</param>
	    ''' <param name="Report">Pointer to the caller-allocated buffer containing the HID report that HidP_InitializeReportForID initializes.</param>
	    ''' <param name="ReportLength">Specifies the size, in bytes, of the HID report located at Report. ReportLength must be equal to the collection's report length for the specified report type, as specified by the XxxReportByteLength members of a collection's HIDP_CAPS structure.</param>
	    ''' <returns>HidP_InitializeReportForID returns one of the following status values:
		''' 	<list type="table">
		''' 		<item>
		''' 			<term>HIDP_STATUS_SUCCESS</term>
		''' 			<description>The report was successfully initialized.</description>
		''' 		</item>
		''' 		<item>
		''' 			<term>HIDP_STATUS_INVALID_PREPARSED_DATA</term>
		''' 			<description>The preparsed data is not valid.</description>
		''' 		</item>
		''' 		<item>
		''' 			<term>HIDP_STATUS_INVALID_REPORT_LENGTH</term>
		''' 			<description>The specified length of the report is not equal to the collection's report length for the specified report type.</description>
		''' 		</item>
		''' 		<item>
		''' 			<term>HIDP_STATUS_INVALID_REPORT_TYPE</term>
		''' 			<description>The report type is not valid.</description>
		''' 		</item>
		''' 		<item>
		''' 			<term>HIDP_STATUS_REPORT_DOES_NOT_EXIST</term>
		''' 			<description>The specified report ID is not valid.</description>
		''' 		</item>
		''' 	</list>
		''' </returns>
		''' <remarks>Initializing a HID report sets all control data to zero or a control's null value, as defined by the USB HID standard. (Sending or receiving a null value indicates that the current value of a control should not be modified.)
		''' 	<para>HidP_InitializeReportForID does the following:</para>
		''' 	<list type="bullet">
		''' 		<item>Sets to zero the bit fields of all buttons and values without null values.</item>
		''' 		<item>Sets the bit field of all controls with null values to their corresponding null value.</item>
		''' 	</list>
		''' 	<para>HidP_InitializeReportForID runs at IRQL &lt;= DISPATCH_LEVEL.</para>
		''' </remarks>
		<DllImport("hid.dll", CharSet := CharSet.Auto, SetLastError := True)> _
	    Function HidP_InitializeReportForID(ByVal ReportType As HIDP_REPORT_TYPE, ByVal ReportID As Byte, ByVal PreparsedData As IntPtr, <[In], Out> ByVal Report As Byte(), ByVal ReportLength As UInteger) As NTSTATUS
	    End Function
	    
	    #End Region
	    
		#Region "Imports (String Properties)"
		
		''' <summary>
		''' The HidD_GetManufacturerString routine returns a top-level collection's embedded string that identifies the manufacturer.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="Buffer">Pointer to a caller-allocated buffer that the routine uses to return the collection's manufacturer string. The routine returns a NULL-terminated wide character string in a human-readable format.</param>
		''' <param name="BufferLength">Specifies the length, in bytes, of a caller-allocated buffer provided at Buffer. If the buffer is not large enough to return the entire NULL-terminated embedded string, the routine returns nothing in the buffer.</param>
		''' <returns>HidD_HidD_GetManufacturerString returns TRUE if it returns the entire NULL-terminated embedded string. Otherwise, the routine returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_GetManufacturerString. Kernel-mode drivers can use an IOCTL_HID_GET_MANUFACTURER_STRING request.
		''' 	<para>The maximum possible number of characters in an embedded string is device specific. For USB devices, the maximum string length is 126 wide characters (not including the terminating NULL character).</para>
		''' </remarks>
		<DllImport("hid.dll", CharSet := CharSet.Unicode, SetLastError := True)> _
		Function HidD_GetManufacturerString(ByVal HidDeviceObject As Integer, <MarshalAs(UnmanagedType.LPTStr)> ByVal Buffer As String, ByVal BufferLength As UInteger) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_GetProductString routine returns the embedded string of a top-level collection that identifies the manufacturer's product.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="Buffer">Pointer to a caller-allocated buffer that the routine uses to return the requested product string. The routine returns a NULL-terminated wide character string.</param>
		''' <param name="BufferLength">Specifies the length, in bytes, of a caller-allocated buffer provided at Buffer. If the buffer is not large enough to return the entire NULL-terminated embedded string, the routine returns nothing in the buffer.</param>
		''' <returns>HidD_GetProductString returns TRUE if it successfully returns the entire NULL-terminated embedded string. Otherwise, the routine returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_GetProductString. Kernel-mode drivers can use an IOCTL_HID_GET_PRODUCT_STRING request.
		''' 	<para>The maximum possible number of characters in an embedded string is device specific. For USB devices, the maximum string length is 126 wide characters (not including the terminating NULL character).</para>
		''' </remarks>
		<DllImport("hid.dll", CharSet := CharSet.Auto, SetLastError := True)> _
		Function HidD_GetProductString(ByVal HidDeviceObject As Integer, <MarshalAs(UnmanagedType.LPTStr)> ByVal Buffer As String, ByVal BufferLength As UInteger) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_GetSerialNumberString routine returns the embedded string of a top-level collection that identifies the serial number of the collection's physical device.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="Buffer">Pointer to a caller-allocated buffer that the routine uses to return the requested serial number string. The routine returns a NULL-terminated wide character string.</param>
		''' <param name="BufferLength">Specifies the length, in bytes, of a caller-allocated buffer provided at Buffer. If the buffer is not large enough to return the entire NULL-terminated embedded string, the routine returns nothing in the buffer.</param>
		''' <returns>HidD_GetSerialNumberString returns TRUE if it successfully returns the entire NULL-terminated embedded string. Otherwise, the routine returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_GetSerialNumberString. Kernel-mode drivers can use IOCTL_HID_GET_SERIALNUMBER_STRING.
		''' 	<para>The maximum possible number of characters in an embedded string is device specific. For USB devices, the maximum string length is 126 wide characters (not including the terminating NULL character).</para>
		''' </remarks>
		<DllImport("hid.dll", CharSet := CharSet.Auto, SetLastError := True)> _
		Function HidD_GetSerialNumberString(ByVal HidDeviceObject As Integer, <MarshalAs(UnmanagedType.LPTStr)> ByVal Buffer As String, ByVal BufferLength As UInteger) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_GetPhysicalDescriptor routine returns the embedded string of a top-level collection that identifies the collection's physical device.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="Buffer">Pointer to a caller-allocated buffer that the routine uses to return the requested physical descriptor.</param>
		''' <param name="BufferLength">Specifies the length, in bytes, of the buffer at Buffer.</param>
		''' <returns>HidD_GetPhysicalDescriptor returns TRUE if it succeeds; otherwise, it returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_GetPhysicalDescriptor. Kernel-mode drivers can use an IOCTL_GET_PHYSICAL_DESCRIPTOR request.</remarks>
		<DllImport("hid.dll", CharSet := CharSet.Unicode, SetLastError := True)> _
		Function HidD_GetPhysicalDescriptor(ByVal HidDeviceObject As Integer, <MarshalAs(UnmanagedType.LPTStr)> ByVal Buffer As String, ByVal BufferLength As UInteger) As Boolean
		End Function
		
		''' <summary>
		''' The HidD_GetIndexedString routine returns a specified embedded string from a top-level collection.
		''' </summary>
		''' <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
		''' <param name="StringIndex">Specifies the device-specific index of an embedded string.</param>
		''' <param name="Buffer">Pointer to a caller-allocated buffer that the routine uses to return the embedded string specified by StringIndex. The routine returns a NULL-terminated wide character string in a human-readable format.</param>
		''' <param name="BufferLength">Specifies the length, in bytes, of a caller-allocated buffer provided at Buffer. If the buffer is not large enough to return the entire NULL-terminated embedded string, the routine returns nothing in the buffer.</param>
		''' <returns>HidD_GetIndexedString returns TRUE if it successfully returns the entire NULL-terminated embedded string. Otherwise, the routine returns FALSE.</returns>
		''' <remarks>Only user-mode applications can call HidD_GetIndexedString. Kernel-mode drivers can use an IOCTL_HID_GET_INDEXED_STRING request.
		''' 	<para>The maximum possible number of characters in an embedded string is device specific. For USB devices, the maximum string length is 126 wide characters (not including the terminating NULL character).</para>
		''' </remarks>
		<DllImport("hid.dll", CharSet := CharSet.Unicode, SetLastError := True)> _
		Function HidD_GetIndexedString(ByVal HidDeviceObject As Integer, ByVal StringIndex As UInteger, <MarshalAs(UnmanagedType.LPTStr)> ByVal Buffer As String, ByVal BufferLength As UInteger) As Boolean
		End Function
		
		#End Region
		
	End Module
	
End Namespace
