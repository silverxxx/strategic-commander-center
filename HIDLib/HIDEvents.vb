'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/24/2007
' Time: 1:13 PM
'----------------------------------------------------------------------

Imports BlueNinjaSoftware.HIDLib.Reports

#Region "Delegate Event Handler Subs"

Public Delegate Sub HIDDataReceivedEventHandler(ByVal sender As Object, ByVal args As HIDDataReceivedEventArgs)

Public Delegate Sub HIDReportReceivedEventHandler(ByVal sender As Object, ByVal args As HIDReportReceivedEventArgs)

#End Region

#Region "Event Argument Classes"

Public Class HIDDataReceivedEventArgs
	Inherits EventArgs
	
	Private _Data As Byte()
	
	Public Sub New(ByVal Data As Byte())
		_Data = Data
	End Sub
	
	Public ReadOnly Property Data() As Byte()
		Get
			Return _Data
		End Get
	End Property
	
End Class

Public Class HIDReportReceivedEventArgs
	Inherits EventArgs
	
	Private _Report As AHIDReport
	
	Public Sub New(Report As AHIDReport)
		_Report = Report
	End Sub
	
	Public ReadOnly Property Report() As AHIDReport
		Get
			Return _Report
		End Get
	End Property
	
End Class

#End Region
