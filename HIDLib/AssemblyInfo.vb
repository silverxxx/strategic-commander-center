'---------------------------------------------------------------------------
' HIDLib: A managed class for HID USB devices.
' Copyright © 2007, 2008 Adam Yarnott, Blue Ninja Software
' 
'This code is free software, and can be used, modified, and distributed by
'anyone, except for commercial purposes (with the exception of the Original
'Purchaser, who is entitled to commercial use), as per the terms of the
'“Commercial Development, Community Benefit License” published by Blue
'Ninja Software. You should have received a copy of this license with this
'code, normally in a file called “License.txt”. You can also obtain it
'from: “http://www.blueninjasoftware.com/cdcbl”.
'
'Unless a specific warranty or service arrangement is made privately between
'parties, which applies only to those parties and to no other party, there
'is NO WARRANTY, express or implied, of any kind, including that of
'merchantability or fitness for a particular use, associated with this
'software.
'---------------------------------------------------------------------------

Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' Information about this assembly is defined by the following
' attributes.
'
' change them to the information which is associated with the assembly
' you compile.

<assembly: AssemblyTitle("HIDLib")>
<assembly: AssemblyDescription("A managed class for HID USB devices.")>
<assembly: AssemblyConfiguration("")>
<assembly: AssemblyCompany("Blue Ninja Software")>
<assembly: AssemblyProduct("HIDLib")>
<assembly: AssemblyCopyright("Copyright © 2007 Adam Yarnott, Blue Ninja Software")>
<assembly: AssemblyTrademark("")>
<assembly: AssemblyCulture("")>

' This sets the default COM visibility of types in the assembly to invisible.
' If you need to expose a type to COM, use <ComVisible(true)> on that type.
<assembly: ComVisible(False)>

' The assembly version has following format :
'
' Major.Minor.Build.Revision
'
' You can specify all values by your own or you can build default build and revision
' numbers with the '*' character (the default):

<assembly: AssemblyVersion("0.1.0.744")>
