'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 8/25/2007
' Time: 8:09 AM
'----------------------------------------------------------------------

Namespace Reports
	
	''' <summary>
	''' An HID Input report (device-to-host).
	''' </summary>
	Public Class HIDInputReport
		Inherits AHIDReport
		
		Protected Friend Sub New(ByVal ReportLength As UShort)
			MyBase.New(ReportLength)
		End Sub
		
		Protected Friend Sub New(ByVal ReportLength As UShort, ByVal ReportID As Byte)
			MyBase.New(ReportLength, ReportID)
		End Sub
		
		Protected Friend Sub New(ByVal ReportLength As UShort, ByVal ReportID As Byte, ByVal ReportData As Byte())
			MyBase.New(ReportLength, ReportID, ReportData)
		End Sub
		
		Protected Friend Sub New(ByVal ReportBuffer As Byte())
			MyBase.New(ReportBuffer)
		End Sub
		
	End Class
	
End Namespace
