'----------------------------------------------------------------------
' Created with SharpDevelop
' Author: Adam Yarnott, Blue Ninja Software
' Copyright (c) Adam Yarnott, Blue Ninja Software.
' Date: 9/5/2007
' Time: 11:58 AM
'----------------------------------------------------------------------



Public Module Utility
	
	Friend Function ParseString(ByVal Source As String) As String
		'TODO: Aren't there restrictions on the byte ranges for this string? Or is that only for the BCD version?
		Source = Source.TrimEnd(Chr(0))
		If Source = ChrW(&h0409) Then
			Debug.WriteLine("String is language code.")
			Source = Nothing
		End If
		Return Source
	End Function
	
	''' <summary>
	''' Prefixes a Hex string with "0x" and left-pads it with 0's to an even number of digits.
	''' </summary>
	''' <param name="HexVal"></param>
	''' <returns></returns>
	Friend Function PadHex(ByVal HexVal As String) As String
		Return "0x" & StrDup(HexVal.Length Mod 2, "0") & HexVal
	End Function
	
	''' <summary>
	''' Prefixes a Hex string with "0x" and left-pads it with 0's to the specified number of digits.
	''' If HexVal is longer than PAdSize, it will be padded to an even number of digits.
	''' </summary>
	''' <param name="HexVal"></param>
	''' <param name="PadSize"></param>
	''' <returns></returns>
	Friend Function PadHex(ByVal HexVal As String, ByVal PadSize As Integer) As String
		Return "0x" & StrDup(IIf(PadSize > HexVal.Length, PadSize - HexVal.Length, HexVal.Length Mod 2), "0") & HexVal
	End Function
	
End Module
